# coding=utf-8
"""
.	Один любой символ, кроме новой строки \n.
?	0 или 1 вхождение шаблона слева
+	1 и более вхождений шаблона слева
*	0 и более вхождений шаблона слева
\w	Любая цифра или буква (\W — все, кроме буквы или цифры)
\d	Любая цифра [0-9] (\D — все, кроме цифры)
\s	Любой пробельный символ (\S — любой непробельнй символ)
\b	Граница слова
[..]	Один из символов в скобках ([^..] — любой символ, кроме тех, что в скобках)
\	Экранирование специальных символов (\. означает точку или \+ — знак «плюс»)
^ и $	Начало и конец строки соответственно
{n,m}	От n до m вхождений ({,m} — от 0 до m)
a|b	Соответствует a или b
()	Группирует выражение и возвращает найденный текст
\t, \n, \r	Символ табуляции, новой строки и возврата каретки соответственно
Regex for email/float/url
"""
import re


def is_valid_email(email):
   regexp_for_email = re.compile(r'\w+\.?\w+@[a-zA-Z]{2,}\.[a-zA-Z]{2,}')
   result = regexp_for_email.match(email)
   if result is None:
       return "Not valid email"
   else:
       return "Valid email"


def is_valid_float(number):
    first_regexp_for_float = re.compile(r'[1-9]+[0-9]+\.\d+')
    second_regexp_for_float = re.compile(r'0\.[0-9]+')
    result = first_regexp_for_float.match(number)
    if result is None:
        if second_regexp_for_float.match(number) is None:
            return "Not valid number"
        else:
            return "Valid number"
    else:
        return "Valid number"


def split_url(url):
    regexp_for_url = re.compile(r'([a-z]+://)([a-zA-Z0-9\.]+\.[a-z]{2,})(/[a-zA-Z0-9/\-\._#]+)(\?.+)?')
    result = regexp_for_url.findall(url)
    return result


if __name__ == "__main__":
    email = "sasha@gmail.com"
    float_numb = '10.0041'
    url = 'https://tproger.ru/translations/regular-expression-python_s/?param1=value1'
    print is_valid_email(email)
    print is_valid_float(float_numb)
    print split_url(url)
