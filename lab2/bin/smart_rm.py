# encoding: utf-8
# module smart_rm

import os
import sys
import argparse
from bin.trash import (
    _is_settings_created,
    get_last_used_trash,
    set_last_used_trash,
    print_file,
    Trash)
from bin.shell_component import (
    delete_permanently,
    delete_by_date_policy,
    delete_by_size_policy,
    delete_by_regex,
    get_choice_from_user,
    print_trash_content,
    restore_files,
    remove_to_trash)
from bin.utils import print_config_file, LOGGER


def start_policy(trash):
    """
    start_policy --> start delete policies
    :return:
    """
    if trash.size_policy:
        delete_by_date_policy(trash)
    if trash.date_policy:
        delete_by_size_policy(trash)


def parse(args, trash):
    """
    parse --> parse argument from console and call necessary method
    :param trash: 
    :param args: argument from terminal
    :return:
    """
    parser = argparse.ArgumentParser(description="Program smart_rm, like rm, but better")

    parser.add_argument(
        "-rs",
        "--restore",
        help="Restore files from your local trash",
        nargs="*"
    )

    parser.add_argument(
        "files",
        nargs="*"
    )

    parser.add_argument(
        "-p",
        "--permanent",
        help="Delete files permanently, ignore trash",
        action='store_true'
    )

    parser.add_argument(
        "--clean",
        help="Clear your local trash",
        action="store_true"
    )

    parser.add_argument(
        "--history",
        help="print trash history on screen",
        action="store_true"
    )

    parser.add_argument(
        "--create_trash",
        help="create trash by path",
        nargs=1
    )

    parser.add_argument(
        "-i",
        "--interactive",
        help="ask you about every delete or remove",
        action="store_true"
    )

    parser.add_argument(
        "-f",
        "--force",
        help="don't print error, and don't ask you about every deleting",
        action="store_false",
        dest="interactive"
    )

    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true"
    )

    parser.add_argument(
        "--silentmode",
        help="On/Off silent mode(don't write trash logs)",
        action="store_true"
    )

    parser.add_argument(
        "--open",
        help="Open trash",
        action="store_true"
    )

    parser.add_argument(
        "--nopreserveroot",
        help="Allow to delete system files and dirs",
        action="store_true"
    )

    parser.add_argument(
        "--preserveroot",
        help="Do not allow to delete system files and dirs",
        action="store_false",
        dest="nopreserveroot"
    )

    parser.add_argument(
        "--sizepolicy",
        help="On/Off auto delete file by size from trash",
        action="store_true"
    )

    parser.add_argument(
        "--datepolicy",
        help="On/Off auto delete file by date from trash",
        action="store_true"
    )

    parser.add_argument(
        "--regex",
        help="Delete file by regular expression",
        nargs=2
    )

    parser.add_argument(
        "--dryrun",
        help="print what will be do",
        action="store_true"
    )

    parser.add_argument(
        "--max_hours",
        help="change config max hours for date policy",
        nargs=1
    )

    parser.add_argument(
        "--max_count",
        help="change config max count for trash",
        nargs=1
    )

    parser.add_argument(
        "--max_size",
        help="change config max size for size policy",
        nargs=1
    )

    parser.add_argument(
        "--config",
        help="print current config file",
        action="store_true"
    )

    result = parser.parse_args(args)

    if result.create_trash:
        trash.create(result.create_trash[0])
        set_last_used_trash(result.create_trash[0])

    if result.recursive:
        def is_recursive():
            """
            is_recursive --> call argument -r
            :return: True
            """
            return True
    else:
        def is_recursive():
            """
            is_recursive --> call argument -r
            :return: False
            """
            return False

    if result.interactive:
        LOGGER.info("Call argument -i")

        def interactive_input():
            """
            interactive_input --> call argument -i
            :return: True
            """
            LOGGER.info("Set interactive True")
            return True
    else:
        def interactive_input():
            """
            interactive_input --> call argument -i
            :return: False
            """
            return False

    if result.nopreserveroot:
        LOGGER.info("Call argument --no-preserve-root")

        def preserve_root():
            """
            preserve_root --> assign argument --preserve-root
            :return: True if get_choice_from_user == 'y'
                    False if get_choice_from_user != 'y'
            """
            print "Are you sure to activate no preserve root, you can probably delete system file(y/n)?"
            if get_choice_from_user():
                LOGGER.info("Set --no-preserve-root")
                return True
            else:
                return False
    else:
        def preserve_root():
            """
            preserve_root --> assign argument --preserve-root
            :return: False
            """
            return False

    if result.dryrun:
        def is_dry_run():
            """
            is_dry_run --> assign argument --dry-run
            :return: True if call argument --dry-run
            """
            return True
    else:
        def is_dry_run():
            """
            is_dry_run --> assign argument --dry-run
            :return: False
            """
            return False

    if result.history:
        LOGGER.info("Call method read_trash_logs")
        path_to_file = os.path.join(trash.path_to_trash, ".trash_logs")
        print_file(path_to_file)

    if result.clean:
        LOGGER.info("Call method clean_trash")
        trash.clean_trash()

    if result.restore:
        LOGGER.info("Call method restore_file")
        restore_files(trash, result.restore)

    if result.regex:
        delete_by_regex(
            trash,
            result.regex[0],
            result.regex[1],
            is_recursive(),
            interactive_input(),
            preserve_root(),
            is_dry_run())

    if result.files and result.permanent:
        delete_permanently(
            trash,
            result.files,
            interactive_input(),
            is_recursive(),
            preserve_root(),
            is_dry_run())

    elif result.files:
        LOGGER.info("Call method remove_to_trash")
        remove_to_trash(
            trash,
            result.files,
            is_recursive(),
            interactive_input(),
            preserve_root(),
            is_dry_run())

    if result.silentmode:
        LOGGER.info("Call method change_silent_mode")
        trash.change_silent_mode()

    if result.max_hours:
        trash.change_max_hours(result.max_hours[0])

    if result.max_count:
        trash.change_max_count(result.max_count[0])

    if result.max_size:
        trash.change_max_size(result.max_size[0])

    if result.config:
        print_config_file(trash)

    if result.open:
        LOGGER.info("Call method open_trash")
        print_trash_content(trash.path_to_trash)

    if result.sizepolicy:
        trash.change_size_policy()

    if result.datepolicy:
        trash.change_date_policy()


def main():
    """
    main --> launch program smart_rm
    :return:
    """
    _is_settings_created()
    LOGGER.info("Start program")

    path_to_trash = get_last_used_trash()
    if path_to_trash is None:
        trash = Trash.create_trash()
    else:
        trash = Trash.create_trash(path_to_trash)

    start_policy(trash)
    parse(sys.argv[1:], trash)


if __name__ == "__main__":
    main()
