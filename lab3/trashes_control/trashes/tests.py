# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from models import (Trash, Tasks)
from forms import AddForm, MyRegistrationForm


class ModelsTestCase(TestCase):
    def setUp(self):
        test_trash = Trash.objects.create(path_to_trash='/home/alex/test_trash',
                                          trash_name='test_trash',
                                          trash_max_size=1,
                                          trash_max_count=1,
                                          trash_max_hours=1,
                                          is_valid=True)

        Tasks.objects.create(trash=test_trash,
                             task=Tasks.TRASH_DELETE,
                             files='1')

        User.objects.create_user(username='Alex', password='Alex123456789')

    def test_models_create(self):
        trash = Trash.objects.get(trash_name='test_trash')
        tasks = Tasks.objects.get(trash=trash)
        self.assertEqual(trash.trash_max_size, 1)
        self.assertEqual(trash, tasks.trash)

    def test_user_auth(self):
        client = Client()
        response = client.get('/login/', {'username': 'Alex', 'password': 'Alex123456789'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_get_nonexistent_page(self):
        client = Client()
        response = client.get('/none')
        self.assertEqual(response.status_code, 404)

    def test_login(self):
        client = Client()
        result_in = client.login(username='Alex', password='Alex123456789')
        self.assertEqual(result_in, True)

    def test_main_page(self):
        client = Client()
        client.login(username='Alex', password='Alex123456789')
        response = client.get('/', {'username': 'Alex'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'trashes.html')

    def test_main_content(self):
        client = Client()
        client.login(username='Alex', password='Alex123456789')
        trash_list = Trash.objects.all()
        response = client.get('/', {'username': 'Alex', 'trash_list': trash_list})
        self.assertEqual(response.context['username'], u'Alex')
        self.assertTemplateUsed(response, 'trashes.html')

    def test_user_not_login(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

    def test_add_form(self):
        path = {'path_to_trash': 'test_trash'}
        form = AddForm(path)
        self.assertFalse(form.is_valid())

    def test_add_form_valid(self):
        trash_data = {
            'path_to_trash': '/home/alex/trash',
            'trash_max_size': 1,
            'trash_max_count': 1,
            'trash_max_hours': 1
        }
        form = AddForm(trash_data)
        self.assertTrue(form.is_valid())

    def test_myregistration_form_valid(self):
        user_data = {
            'username': 'Sasha',
            'password1': 'Sasha120997',
            'password2': 'Sasha120997',
            'email': 'sashastresh@gmail.com'
        }
        form = MyRegistrationForm(user_data)
        self.assertTrue(form.is_valid())

    def test_myregistration_form_not_valid(self):
        user_data = {
            'username': 'Sasha',
            'password1': 'Sasha120997',
            'password2': 'Sasha120997',
            'email': ''
        }
        form = MyRegistrationForm(user_data)
        self.assertFalse(form.is_valid())

    def test_get_about_page(self):
        self.client.login(username='Alex', password='Alex123456789')
        response = self.client.get('/about/', {'username': 'Alex'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'about.html')

    def test_about_page(self):
        self.client.login(username='Alex', password='Alex123456789')
        response = self.client.get('/help/', {'username': 'Alex'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'help.html')

    def test_trash_details(self):
        trash = get_object_or_404(Trash, trash_name='test_trash')
        self.client.login(username='Alex', password='Alex123456789')
        response = self.client.get('/trash/{}/'.format(trash.id), {'username': 'Alex'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'trash.html')

    def test_correct_template(self):
        self.client.login(username='Alex', password='Alex123456789')
        result = self.client.get(reverse('render_user_cabinet'))
        self.assertTemplateUsed(result, 'user_cabinet.html')

    def test_user_cabinet(self):
        self.client.login(username='Alex', password='Alex123456789')
        response = self.client.get(reverse('render_user_cabinet'))
        self.assertEqual(response.status_code, 200)

    #TODO --> READ ABOUT SELENIUM
