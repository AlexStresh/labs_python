import unittest
import os
from bin.shell_component import (
    remove_to_trash,
    restore_files,
    delete_by_regex,
    delete_permanently
)
from bin.trash import (
    Trash,
    delete_recursive,
    can_remove,
    get_count,
    get_file_size,
    is_valid_trash,
    get_info_from_config,
    create_config,
    is_exist
)


class TestRm(unittest.TestCase):

    def setUp(self):
        self.path_to_tests = os.path.abspath("smrm_tests")
        self.path_to_trash = os.path.join(self.path_to_tests, "test_trash")
        self.trash = Trash.create_trash(self.path_to_trash)
        if not is_exist( 'smrm_tests' ):
            os.mkdir( 'smrm_tests' )

    def test_remove_to_trash_file(self):
        path_to_file = os.path.join(self.path_to_tests, "file_test_1")
        with open(path_to_file, 'w') as test_file:
            test_file.write("test")
        remove_to_trash(self.trash, [path_to_file])
        self.assertFalse(os.path.isfile("test_file"))
        self.trash.clean_trash()

    def test_remove_to_trash_empty_dir(self):
        path_to_empty_dir = os.path.join(self.path_to_tests, "empty_dir_1" )
        os.mkdir(path_to_empty_dir)
        remove_to_trash(self.trash, [path_to_empty_dir], is_recursive=True)
        self.assertFalse(os.path.isdir(path_to_empty_dir))
        self.trash.clean_trash()

    def test_remove_to_trash_dir(self):
        path_to_dir = os.path.join(self.path_to_tests , "dir_1")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        remove_to_trash(self.trash, [path_to_dir], is_recursive=True)
        self.assertFalse(os.path.isdir(path_to_dir))
        self.trash.clean_trash()

    def test_remove_to_trash_dir_without_r(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_2")
        os.mkdir(path_to_dir)
        remove_to_trash(self.trash, [path_to_dir])
        self.assertTrue(os.path.isdir(path_to_dir), "Can't move dir to trash")
        remove_to_trash(self.trash, [path_to_dir], is_recursive=True)
        self.trash.clean_trash()

    def test_remove_to_trash_dry_run(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_3")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        remove_to_trash(self.trash, [path_to_dir])
        self.assertTrue(os.path.isdir(path_to_dir))
        remove_to_trash(self.trash, [path_to_dir], is_recursive=True)
        self.trash.clean_trash()

    def test_permanent_deleting_file(self):
        path_to_file = os.path.join(self.path_to_tests, "file_test_2")
        with open(path_to_file, 'w') as test_file:
            test_file.write("test")
        delete_permanently(self.trash, [path_to_file])
        self.assertFalse(os.path.isfile(path_to_file))
        self.trash.clean_trash()

    def test_permanent_deleting_empty_dir(self):
        path_to_empty_dir = os.path.join(self.path_to_tests, "empty_dir_2")
        os.mkdir(path_to_empty_dir)
        delete_permanently(self.trash, [path_to_empty_dir], is_recursive=True)
        self.assertFalse(os.path.isdir(path_to_empty_dir))
        self.trash.clean_trash()

    def test_permanent_deleting_dir(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_4")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        delete_permanently(self.trash, [path_to_dir], is_recursive=True)
        self.assertFalse(os.path.isdir(path_to_dir))

    def test_permanent_deleting_dir_without_r(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_5")
        os.mkdir(path_to_dir)
        delete_permanently(self.trash, [path_to_dir])
        self.assertTrue(os.path.isdir(path_to_dir), "Can't move dir to trash")
        delete_permanently(self.trash, [path_to_dir], is_recursive=True)

    def test_permanent_deleting_dry_run(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_6")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        delete_permanently(self.trash, [path_to_dir], is_recursive=True, is_dry_run=True)
        self.assertTrue(os.path.isdir(path_to_dir), "Can't move dir to trash")
        delete_permanently(self.trash, [path_to_dir], is_recursive=True)

    def test_delete_by_regex(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_7")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        delete_by_regex(self.trash, path_to_dir, "[1-9]", recursive=True)
        self.assertFalse(os.listdir(path_to_dir) == 0)
        delete_permanently(self.trash, [path_to_dir], is_recursive=True)

    def test_clean_trash(self):
        self.trash.clean_trash()
        trash_count = get_count(self.trash.path_to_trash)
        self.assertTrue(trash_count <= 3)

    def test_restore_file(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_8")
        path_to_file = os.path.join(self.path_to_tests, "test_file_3")
        os.mkdir(path_to_dir)
        with open(path_to_file, 'w') as test_file:
            test_file.write("test")
        remove_to_trash(self.trash, [path_to_file])
        remove_to_trash(self.trash, [path_to_dir], is_recursive=True)
        restore_files(self.trash, ["test_file_3", "dir_8"])
        self.assertTrue(os.path.isdir(path_to_dir) and os.path.isfile(path_to_file))
        delete_permanently(self.trash, [path_to_file, path_to_dir], is_recursive=True)

    def test_move_to_trash(self):
        path_to_empty_dir = os.path.join(self.path_to_tests, "empty_dir_9" )
        os.mkdir(path_to_empty_dir)
        self.trash.move_to_trash([path_to_empty_dir])
        self.assertFalse(os.path.isdir(path_to_empty_dir))

    def test_move_to_trash_two_dir(self):
        first_dir = os.path.join(self.path_to_tests, "1")
        second_dir = os.path.join(self.path_to_tests, "2")
        self.trash.move_to_trash([first_dir, second_dir])
        self.assertFalse(is_exist(first_dir) and is_exist(second_dir))

    def test_move_to_trash_dirs_with_files(self):
        path_to_first_dir = os.path.join(self.path_to_tests, "1")
        path_to_second_dir = os.path.join(self.path_to_tests, "2")
        os.mkdir(path_to_first_dir)
        os.mkdir(path_to_second_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_first_dir, str(tmp)))
            os.mkdir(os.path.join(path_to_second_dir, str(tmp)))
        self.trash.move_to_trash([path_to_second_dir, path_to_first_dir])
        self.assertFalse(is_exist(path_to_first_dir) and is_exist(path_to_second_dir))

    def test_restore_files(self):
        path_to_first_dir = os.path.join(self.path_to_tests, "1")
        path_to_second_dir = os.path.join(self.path_to_tests, "2")
        os.mkdir(path_to_first_dir)
        os.mkdir(path_to_second_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_first_dir, str(tmp)))
            os.mkdir(os.path.join(path_to_second_dir, str(tmp)))
        self.trash.move_to_trash([path_to_second_dir, path_to_first_dir])
        self.trash.restore_file(["1", "2"], )
        self.assertTrue(is_exist(path_to_first_dir) and is_exist(path_to_second_dir))
        delete_recursive(path_to_first_dir)
        delete_recursive(path_to_second_dir)

    def test_move_to_new_trash(self):
        path_to_empty_dir = os.path.join(self.path_to_tests, "empty_dir_10")
        path_to_trash = os.path.join(self.path_to_tests, "local_trash")
        os.mkdir(path_to_empty_dir)
        self.trash = Trash.create_trash(path_to_trash)
        self.trash.move_to_trash([path_to_empty_dir])
        path_to_trash_config = os.path.join(path_to_trash, ".config")
        self.assertTrue(os.path.isfile(path_to_trash_config))
        self.trash.clean_trash()

    def test_create_trash(self):
        path_to_trash = os.path.join(self.path_to_tests, "_trash_")
        path_to_trash_config = os.path.join(path_to_trash, ".config")
        self.trash = Trash.create_trash(path_to_trash)
        self.assertTrue(os.path.isfile(path_to_trash_config))

    def test_get_count(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_count")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        count = get_count(path_to_dir)
        self.assertTrue(count == 9)
        delete_recursive(path_to_dir)

    def test_get_size(self):
        path_to_dir = os.path.join(self.path_to_tests, "dir_size")
        os.mkdir(path_to_dir)
        for tmp in range(1, 10):
            os.mkdir(os.path.join(path_to_dir, str(tmp)))
        size = get_file_size(path_to_dir)
        self.assertTrue(size == 0)
        delete_recursive(path_to_dir)

    def test_is_can_remove(self):
        check = can_remove(
            self.trash,
            self.path_to_tests,
            is_preserve_root=False,
            is_recursive=False
        )
        self.assertTrue(check)

    def test_is_can_remove_sys(self):
        check = can_remove(
            self.trash,
            self.path_to_tests,
            is_preserve_root=True,
            is_recursive=False
        )
        self.assertTrue(check)

    def test_is_can_remove_dir(self):
        check = can_remove(
            self.trash,
            self.path_to_tests,
            is_preserve_root=True,
            is_recursive=True
        )
        self.assertTrue(check)

    def test_is_valid_trash(self):
        self.assertTrue(is_valid_trash(self.path_to_trash))

    def test_is_not_valid_trash(self):
        self.assertFalse(is_valid_trash(self.path_to_tests))

    def test_create_config(self):
        content = get_info_from_config(os.path.join(self.path_to_trash, ".config"))
        local_config = os.path.join(self.path_to_tests, ".localconfig" )
        create_config(local_config, content)
        local_content = get_info_from_config(local_config)
        self.assertTrue(content == local_content)
        delete_recursive(local_config)
        self.trash = Trash.create_trash(self.path_to_tests )

    def test_change_max_size(self):
        max_size = self.trash.max_size
        self.trash.change_max_size(max_size+1)
        self.assertTrue(max_size == self.trash.max_size)

    def test_change_max_count(self):
        max_count = self.trash.max_count
        self.trash.change_max_count(max_count+1)
        self.assertTrue(max_count+1 == self.trash.max_count)

    def test_change_max_hours(self):
        max_hours = self.trash.max_hours
        self.trash.change_max_hours(max_hours+1)
        self.assertTrue(max_hours+1 == self.trash.max_hours)


if __name__ == '__main__':
    if not is_exist('smrm_tests'):
        os.mkdir('smrm_tests')
    unittest.main()
