"""
default dict
"""


class DefaultDict(object):

    def __init__(self):
        self.def_dict = dict()

    def __setitem__(self, key, value):
        self.def_dict[key] = value

    def __getitem__(self, item):
        if item in self.def_dict:
            return self.def_dict[item]
        else:
            self.def_dict[item] = DefaultDict()
            return self.def_dict[item]

    def __delitem__(self, key):
        del self.def_dict[key]

    def __contains__(self, item):
        return item in self.def_dict

    def __str__(self):
        ans = ['{\n']
        for items in self.def_dict.items():
            ans.append(str(items[0]))
            ans.append(": ")
            ans.append(str(items[1]))
            ans.append(", \n")

        if len(ans) > 1:
            ans = ans[:-1]

        ans.append('\n}')
        return "".join(ans)

if __name__ == "__main__":
    defdict = DefaultDict()
    defdict.def_dict['a'] = {'name': 'stresh'}
    defdict.def_dict['a']['c'] = 5
    defdict.def_dict['b'] = {'name': 'stresh'}
    defdict.def_dict['b']['c'] = {'cur_name': 'alex'}
    defdict.def_dict['b']['c']['d'] = 12

    print defdict
