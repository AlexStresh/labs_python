# usr/bin/env python2
# -*-coding:utf-8-*-
"""

"""
import sys
import os
import time
import logging
from datetime import datetime
from platform import system as system_name
from getkey import getkey
from os import system as system_call
from smart_rm_action import (
    get_info_from_config,
    get_file_size)

BYTE_TO_MB = 1024**2

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
                    datefmt="%m-%d %H:%M",
                    filename="/home/alex/.config/smart_rm/.log",
                    filemode="w")
LOGGER = logging.getLogger(__name__)


def refresh_screen():
    command = "-cls" if system_name().lower() == "windows" else "clear"
    system_call(command)


def get_choice_from_user():
    """
    Get yes or no from user
    :return: True - if user input y(yes)
             False - if user input n(no)
    """
    LOGGER.debug("Call get_choice")
    choice = getkey()
    LOGGER.debug("Answer : %s", choice)
    return bool(choice.lower() == "y")


def print_config_file(trash):
    """
    Print config file on the screen
    """
    LOGGER.debug("Call open_config_file")
    configs = get_info_from_config(os.path.join(trash.path_to_trash, ".config"))
    tmp = 1
    for key in configs.iterkeys():
        s = "{key}:{value}".format(key=key, value=configs[key])
        print "{tmp}) {config_string}".format(tmp=tmp, config_string=s)
        tmp += 1


def write_trash_logs(trash, info):
    """
    write_trash_logs --> write info about process in log file
    :param trash: 
    :param info: info about process
    """
    try:
        size = os.statvfs(trash.path_to_trash)
        available_size = size.f_bsize * size.f_bavail / BYTE_TO_MB

        with(open(os.path.join(trash.path_to_trash, ".trash_logs"), "a")) as logs:
            logs.write("[Trash]: {info} [{date}]\n".format(
                info=info,
                date=str(datetime.
                         fromtimestamp(time.time()).
                         strftime("%Y-%m-%d %H:%M:%S"))))

            logs.write("Available size {} MB\n".format(available_size))
            logs.write("Available size for trash: {} MB\n".format(
                int(trash.max_size)
                - get_file_size(trash.path_to_trash)/BYTE_TO_MB))

    except IOError:
        LOGGER.error("Error while write trash logs")
        sys.exit(-1)
