"""
My xrange
"""


class my_xrange(object):
    start = 0
    stop = 0
    step = 1

    def __init__(self, *args, **kwargs):

        if len(args) == 1:
            self.stop = args[0]
        elif len(args) == 2:
            self.start = args[0]
            self.stop = args[1]
        elif len(args) == 3:
            self.start = args[0]
            self.stop = args[1]
            self.step = args[2]
        else:
            raise ValueError('To many value to __init__')

        self.count = self.stop

    def __len__(self):
        return self.stop

    def __iter__(self):
        start = self.start
        stop = self.stop
        step = self.step
        while start < stop:
            yield start
            start += step
        raise StopIteration()

if __name__ == "__main__":
    rng = my_xrange(10)
    a = [val for val in rng]
    print a
    b = [val for val in rng]
    print b
    print len(rng)
