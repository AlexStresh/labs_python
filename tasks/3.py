"""
Unic list
"""
import os
import re


class UnicList(object):

    def __init__(self, path_to_file=None):
        if path_to_file is None:
            self.unic_list = []
        else:
            path_to_file = os.path.abspath(path_to_file)
            if os.path.isfile(path_to_file):
                with open(path_to_file, 'r') as text:
                    elements = [int(element) for element in text.readlines()]
                elements = set(elements)
                self.unic_list = list(elements)
            else:
                self.unic_list = []

    def __get_list(self):
        return object.__getattribute__(self, 'unic_list')

    def __set_list(self, value):
        u_list = self.__get_list()
        u_list.append(value)
        u_list = set(u_list)
        self.unic_list = list(u_list)

    def __del_list(self):
        if self.unic_list is None:
            raise AttributeError
        else:
            del self.unic_list

    def add(self, values):
        if isinstance(values, list):
            for elem in values:
                self.__set_list(elem)
                print "Add", elem
        elif not isinstance(values, dict):
            self.__set_list(values)

    def remove(self, elem):
        u_list = self.__get_list()
        try:
            u_list.remove(elem)
            print "Remove", elem
        except ValueError:
            print "There is no such element"
        finally:
            self.unic_list = u_list

    def list(self):
        u_list = self.__get_list()
        for elem in u_list:
            print u_list, ' '

    def save(self, path_to_save):
        path = os.path.abspath(path_to_save)
        with open(path, 'w+') as save_file:
            save_file.truncate()
            for elem in self.__get_list():
                save_file.write(str(elem) + '\n')
        print "Saved to", path

    def load(self, path_to_file):
        path = os.path.abspath(path_to_file)
        with open(path, 'w+') as load_file:
            lines = [line.strip().replace('\n', '') for line in load_file.readlines()]
        print "Load file"

    def find(self, values):
        u_list = self.__get_list()
        find = []
        if not isinstance(values, list):
            values = [values]

        for elem in values:
            if elem in u_list:
                find.append(elem)

        return find

    def find_by_grep(self, regex):
        find = []
        u_list = self.__get_list()

        for elem in u_list:
            if re.match(regex, str(elem)):
                find.append(elem)
        return find

if __name__ == "__main__":
    u_list = UnicList()
    u_list.add([1, 2, 3, 3, 4, 5, 'asd', '1as'])
    print u_list.find([1, 2, 'str'])
    u_list.remove(1)
    u_list.save('12')
