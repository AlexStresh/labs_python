# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-01 17:50
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('trashes', '0013_auto_20170801_1730'),
    ]

    operations = [
        migrations.AddField(
            model_name='tasks',
            name='coments',
            field=models.CharField(default='', max_length=256),
        ),
        migrations.AlterField(
            model_name='tasks',
            name='create_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 8, 1, 17, 50, 24, 827552, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='trash',
            name='add_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 8, 1, 17, 50, 24, 826762, tzinfo=utc)),
        ),
    ]
