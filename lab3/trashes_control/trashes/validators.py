from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def validate_positive_numb(numb):
    if numb <= 0:
        raise ValidationError(
            _('%(value)s is not positive number'),
            params={'value': numb},
        )
