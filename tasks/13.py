"""
Filter sequence
"""


class FilterSequence(object):

    def __init__(self, seq, filters=None):
        if not hasattr(seq, '__iter__'):
            raise TypeError('Must be only iter object')

        self.seq = seq

        if filters is None:
            self.filters = None
        else:
            self.filters = filters

    def __iter__(self):

        if self.filters is None:
            return self.seq.__iter__()
        else:
            return [val for val in self.seq.__iter__() if self.filters(val)]

    def __str__(self):
        string = ''
        for elem in self.__iter__():
            string += str(elem) + ' '

        return string

    def filter(self, func):
        return FilterSequence(self, func)


def filter_numb(elem):
    if elem % 2:
        return True
    else:
        return False

if __name__ == "__main__":
    seq = [val for val in xrange(10)]
    sq = FilterSequence(seq, filter_numb)
    print sq
