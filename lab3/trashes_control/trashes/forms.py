import os
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from models import Trash
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from bin.trash import (
    is_exist,
    _is_sys_dir
)


class MyRegistrationForm(UserCreationForm):
    """
    Class MyRegistrationForm - Use to create and save User to db. 
    Extends on UserCreationForm.
    """
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = {'username', 'password1', 'password2', 'email'}

    def save(self, commit=True):
        """
        MyRegistrationForm.save --> create and save model object to db
        :param commit: add object without change
        :return: object MyRegistrationForm
        """
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


class AddForm(ModelForm):
    """
    Class AddForm - Use to create and save Trash object to db. 
    Extends on ModelForm.
    """
    class Meta:
        model = Trash
        fields = ['path_to_trash', 'trash_max_size', 'trash_max_count', 'trash_max_hours']

    def clean_path_to_trash(self):
        """
        clean_path_to_trash --> check path_to_trash to valid, if not valid
                                raise ValidationError and send error to form
        :return: path_to_trash - absolute path to trash
        """
        path_to_trash = self.cleaned_data['path_to_trash']

        if path_to_trash.find('/') == -1:
            path_to_trash = os.path.join(os.path.expanduser('~'), path_to_trash)

        path_to_trash = os.path.abspath(path_to_trash)
        if _is_sys_dir(path_to_trash):
            raise ValidationError(
                _('%(value)s is not correct path to trash'),
                params={'value': path_to_trash},
            )
        else:
            if not is_exist(path_to_trash):
                try:
                    os.mkdir(path_to_trash)
                except:
                    raise ValidationError(
                        _('%(value)s is not correct path to trash'),
                        params={'value': path_to_trash},
                    )
        return path_to_trash


class RestoreForm(forms.Form):
    RESTORE_CHOICES = (
        (True, 'Restore with replacement'),
        (False, 'Restore without replacement')
    )
    solve_problem = forms.ChoiceField(choices=RESTORE_CHOICES, label="", initial='', widget=forms.Select(), required=True)
