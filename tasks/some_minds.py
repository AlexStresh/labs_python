import collections


class Property(object):
    def __init__(self, fget=None, fset=None, fdel=None):
        self.fget = fget
        self.fset = fset
        self.fdel = fdel

    def __get__(self, instance, owner):
        if instance is None:
            return self
        if self.fget is None:
            raise AttributeError('no attr')
        return self.fget(instance)

    def __set__(self, instance, value):
        if self.fset is None:
            raise AttributeError('no attr')
        self.fset(instance, value)

    def __delete__(self, instance):
        if self.fdel is None:
            raise AttributeError('no attr')
        self.fdel(instance)


class cashed_property(object):

    def __init__(self, func):
        self.func = func
        self.name = func.__name__

    def __get__(self, instance, owner):
        if self.name not in instance.__dict__:
            result = instance.__dict__[self.name] = self.func(instance)
            print 'New'
            return result
        print 'Old'
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        del instance.__dict__[self.name]


def cashed_decorator(func):
    logger = {}

    def wrapper(*args):
        key = ""
        key = key.join([str(elem)+" " for elem in args])
        if key in logger:
            return logger[key]
        else:
            result = func(*args)
            logger.update({key: result})
            return result
    return wrapper


def niter(iterable, n=2):
    it = iter(iterable)
    deques = [collections.deque() for i in range(n)]

    def gen(mydeque):
        while True:
            if not mydeque:
                newval = next(it)
                for d in deques:
                    d.append(newval)

            yield mydeque.popleft()

    return tuple(gen(d) for d in deques)


@cashed_decorator
def test_func(a, b, c):
    return a + b + c


if __name__ == '__main__':
    a = niter([1, 2, 3], 5)
    print type(a)