# -*- coding: utf-8 -*-
import re
import os
import json


DEFAULT_PATH = os.path.expanduser('~')


def get_dir_content(path_to_dir):
    json_data = '['
    files = [os.path.join(path_to_dir, file_name)
             for file_name in get_filtered_files(path_to_dir)]
    dir_content = match_file_type(files)
    json_data += json.dumps(dir_content)
    json_data += ']'
    return json_data


def find_files_with_regex(regex, files):
    """
    Find and add files that math to regex in dirs
    :param regex: regular expression
    :param files: list of dirs
    :return: files that math to regex
    """
    files_to_delete = []
    if not files:
        files = [
            path_to_file for path_to_file in os.listdir(DEFAULT_PATH)
            if re.match(regex, path_to_file)
        ]

        files = [
            os.path.join(DEFAULT_PATH, path_to_file)
            for path_to_file in files
        ]

        files_to_delete = [
            path_to_file for path_to_file in files
            if os.path.isfile(path_to_file)
        ]
    else:
        for path_to_file in files:
            if os.path.isdir(path_to_file):
                dir_content = [
                    file for file in os.listdir(path_to_file)
                    if re.match(regex, file)]

                dir_content = [
                    os.path.join(path_to_file, file_name)
                    for file_name in dir_content
                ]

                files_to_delete += [
                    file_name for file_name in dir_content
                    if os.path.isfile(file_name)
                ]

    return files_to_delete


def match_file_type(files):
    """
    Indentify file's type
    :param files: 
    :return: dir with match files
    """
    dir_context = {}

    for file_name in files:
        if os.path.isdir(file_name):
            dir_context.update({file_name: True})

        else:
            dir_context.update({file_name: False})

    return dir_context


def get_filtered_files(path_to_dir):
    """
    Get files from dir without hidden files and dirs from list
    :param path_to_dir: 
    :return: 
    """
    dir_path = os.path.abspath(path_to_dir)
    files = [os.path.join(path_to_dir, path_to_file)
             for path_to_file in os.listdir(dir_path)
             if not path_to_file.startswith('.')]

    return files

