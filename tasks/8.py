"""
Class Logger
"""


class Logger(object):
    log = []

    def __getattribute__(self, attr):
        attrs = super(Logger, self).__getattribute__(attr)
        if callable(attrs):
            def logging(*args, **kwargs):
                result = attrs(*args, **kwargs)
                self.log.append([attr, args, kwargs, result])
                return result
            return logging
        else:
            return attrs


if __name__ == "__main__":
    pass
