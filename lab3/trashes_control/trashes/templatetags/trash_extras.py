from django import template

from bin.trash import get_count, get_file_size, BYTE_TO_KB

register = template.Library()


@register.filter
def get_file_name(path):
    name = path.split('/')[-1]
    return name


@register.filter
def get_trash_count(path_to_trash):
    trash_count = get_count(path_to_trash) - 3
    if trash_count < 0:
        trash_count = 0
    return trash_count


@register.filter
def get_trash_size(path_to_trash):
    trash_size = float(get_file_size(path_to_trash) / BYTE_TO_KB**2)
    return trash_size
