# encoding: utf-8
# module shell_component
"""
This module provides work with Trash from console. Ypu can use one Trash in one time.
Trash create by class Trash from module smart_rm_action.
Available function:
    1. print_trash_content(path) --> print Trash(path_to_trash = path) content
    2. delete_by_size_policy() --> delete files from trash by size
    3. delete_by_date_policy() --> delete files from trash by date
    4. delete_by_regex(path, regex) --> delete files from subdir
        (path_to_subdir = path) be regex
    5. delete_with_confirmation(files) --> get user choice to delete file
    6. delete_permanently(files) --> delete files from computer(y can't restore it)
    7. remove_to_trash(files) --> delete files to Trash(you can restore it)
    8. restore_files(files) --> restore files from Trash
If you want to create Trash and change Trash settings, use module smart_rm_action.
"""
import re
import fnmatch
import os
import sys
from datetime import timedelta, datetime

from trash import (
    delete_recursive,
    get_file_size,
    can_remove,
    get_count,
    _write_file_info_json,
    is_exist,
    _load_info_json,
    print_file,
    is_valid_trash
)
from bin.utils import (
    refresh_screen,
    get_choice_from_user,
    write_trash_logs,
    LOGGER
)


def print_trash_content(path_to_trash):
    """
    Print trash's content on the screen
    :type path_to_trash: object
    """
    if not is_valid_trash(path_to_trash):
        raise Exception("Not valid trash")

    try:
        trash_files = os.listdir(path_to_trash)
        info = _load_info_json(path_to_trash)
        path_to_trash_content = os.path.join(path_to_trash, ".trash_count")

        if info:
            for trash_file in trash_files:
                path_file = os.path.join(path_to_trash, trash_file)
                file_size = os.path.getsize(path_file) / 1024
                for field in info:
                    with open(path_to_trash_content, "a") as trash_content:
                        if field["UUID"] == trash_file and os.path.isdir(path_file):
                            trash_content.write("{file} : dir : {size} KB\n".format(
                                file=field["name"],
                                size=file_size))
                        elif field["UUID"] == trash_file and os.path.isfile(path_file):
                            trash_content.write("{file} : file : {size} KB\n".format(
                                    file=field["name"],
                                    size=file_size))
            print_file(path_to_trash_content)
            delete_recursive(path_to_trash_content)
        else:
            print "Trash empty"

    except IOError:
        sys.exit(1)


def _print_deleting_process(max_count, delete_count, max_size, delete_size):
    """
    print deleting process on the screen
    :param max_count: total number of files
    :param delete_count: number of deleted files 
    :param max_size: total size of files in B
    :param delete_size: size of deleted files in B
    """
    refresh_screen()
    print "Deleting --> {delete_count}/{max_count} {delete_size}/{max_size} B".format(
        delete_count=delete_count,
        max_count=max_count,
        delete_size=delete_size,
        max_size=max_size)


def delete_by_size_policy(trash):
    """
    Delete files from trash by size policy
    """
    try:
        path_to_trash = trash.path_to_trash
        max_size = path_to_trash
        trash_size = get_file_size(path_to_trash)

        if trash_size > max_size:
            trash.clean_trash(path_to_trash)
            write_trash_logs(trash, "Clear trash by size policy")

    except IOError:
        raise Exception("Error while deleting by size policy")


def delete_by_date_policy(trash):
    """
    Delete files from trash by date_policy
    :param trash: 
    """
    try:

        datetime_now = datetime.now()
        path_to_trash = trash.path_to_trash
        info = _load_info_json(path_to_trash)
        hour = timedelta(hours=trash.max_hours)
        files = []

        for field in info:
            delete_time = datetime.strptime(field["time"], "%Y.%m.%d %H:%M:%S")
            if datetime_now - delete_time >= hour:
                delete_recursive(os.path.join(path_to_trash, field["UUID"]))
                files.append(field["name"])
                write_trash_logs(trash, "Delete file {} by date policy".format(field["name"]))
                info.remove(field)
            _write_file_info_json(path_to_trash, info)

        return files

    except IOError:
        raise Exception("Error while deleting by date policy")


def delete_by_regex(trash,
                    path_to_dir,
                    regular_exp,
                    recursive=False,
                    input_interactive=False,
                    is_preserve_root=False,
                    is_dry_run=False):
    """
    Delete files from dir by regular expression
    :param trash: 
    :param regular_exp: 
    :param path_to_dir: 
    :param recursive: atribute -r
    :param input_interactive: atribute -i
    :param is_preserve_root:
    :param is_dry_run: atribute --dry-run
    """
    path_to_dir = os.path.abspath(path_to_dir)
    regular_exp = fnmatch.translate(regular_exp)
    regular_exp = re.compile(regular_exp)

    if os.path.isdir(path_to_dir):

        try:

            for root, dirs, files in os.walk(path_to_dir, topdown=False):
                for file_name in files:
                    if re.search(regular_exp, file_name):
                        delete_permanently(
                            trash,
                            [os.path.join(root, file_name)],
                            input_interactive,
                            recursive,
                            is_preserve_root,
                            is_dry_run)

                if recursive:
                    for dir_name in dirs:
                        if re.search(regular_exp, dir_name):
                            delete_permanently(
                                trash,
                                [os.path.join(root, dir_name)],
                                input_interactive,
                                recursive,
                                is_preserve_root,
                                is_dry_run)

        except re.error:
            raise Exception("Wrong regular expression")

    else:
        raise Exception("There is no such dir")


def delete_with_confirmation(trash,
                             file_name,
                             is_recursive=False,
                             is_preserve_root=False,
                             is_dry_run=False):
    """
    Delete file with user interface 
    :param trash: 
    :param file_name: path to file which 
    :param is_recursive: atribute -r, which call recursuve delete 
    :param is_preserve_root: atribute --preserve-root - allow to delete sys dir and files
    :param is_dry_run: atribute --dry-run - print what will be do
    """
    if os.path.isfile(file_name):
        if can_remove(trash, file_name, is_preserve_root, is_recursive):
            print "Delete file {}?".format(file_name)
            if get_choice_from_user():
                if not is_dry_run:
                    os.remove(file_name)
                    write_trash_logs(trash, "Delete file {}".format(file_name))

        else:
            print "Can't remove {}".format(file_name)

    elif os.path.isdir(file_name) and is_recursive:
        if can_remove(trash, file_name, is_preserve_root, is_recursive):
            if not is_dry_run:
                delete_recursive_with_confirmation(
                    trash.path_to_trash,
                    file_name,
                    is_preserve_root,
                    is_dry_run,
                    is_recursive
                )

    elif os.path.isdir(file_name) and not is_recursive:
        print "Use argument -r to delete dirs"

    elif os.path.islink(file_name):
        if not is_preserve_root:
            print "Delete link {}?".format(file_name)
            if get_choice_from_user():
                if not is_dry_run:
                    os.unlink(file_name)
                    write_trash_logs(trash, "Unlink {}".format(file_name))

    else:
        print "Can't to delete such type file"


def delete_permanently(trash,
                       files,
                       interactive=False,
                       is_recursive=False,
                       is_preserve_root=False,
                       is_dry_run=False):
    """
    Delete file ignore trash
    :param trash: 
    :param files: paths to files
    :param interactive: atribute -i - prompt before every removal
    :param is_recursive: atribute -r, which call recursuve delete 
    :param is_preserve_root: atribute --preserve-root - allow to delete sys dir and files
    :param is_dry_run: atribute --dry-run - print what will be do
    """
    path_to_trash = trash.path_to_trash
    max_size = 0
    current_size = 0
    max_count = 0
    current_count = 0

    try:

        for path_to_file in files:
            file_name = os.path.abspath(path_to_file)
            if is_exist(file_name):
                max_size += int(get_file_size(file_name)) * 1024 * 1024
                max_count += int(get_count(file_name))

        for file_name in files:
            file_name = os.path.abspath(file_name)

            if interactive:
                current_count += get_count(file_name)
                current_size += get_file_size(file_name) * 1024 * 1024
                _print_deleting_process(max_count, current_count, max_size, current_size)
                delete_with_confirmation(path_to_trash,
                                         file_name,
                                         is_recursive,
                                         is_preserve_root,
                                         is_dry_run)

            else:
                if can_remove(trash, file_name, is_preserve_root, is_recursive):
                    if not is_dry_run:
                        if (os.path.isdir(file_name) and is_recursive) or os.path.isfile(file_name):
                            delete_recursive(file_name)
                            write_trash_logs(trash, "Delete {}".format(file_name))

                        elif os.path.islink(file_name):
                            os.unlink(file_name)

    except IOError:
        sys.exit(1)


def delete_recursive_with_confirmation(trash,
                                       path_to_file,
                                       is_preserve_root=False,
                                       is_dry_run=False,
                                       is_recursive=False):
    """
    Recursive delete with user interface
    :param trash: 
    :param is_recursive: 
    :param is_dry_run:
    :param is_preserve_root:
    :param path_to_file: absolute path to files or dirs
    :return:
    """
    LOGGER.debug("Call method ui_delete_recursive")
    path = os.path.abspath(path_to_file)
    if can_remove(trash, path, is_preserve_root, is_recursive):
        if os.path.isdir(path) and os.listdir(path) == []:
            print "Delete dir {path}?".format(path=path)
            if get_choice_from_user():
                if not is_dry_run:
                    os.rmdir(path)

        if os.path.isfile(path):
            print "Delete file {path}?".format(path=path)
            if get_choice_from_user():
                if not is_dry_run:
                    os.remove(path)

        elif os.path.isdir(path):
            for root, dirs, files in os.walk(path, topdown=False):
                for file_name in files:
                    print "Delete file {path}?".format(path=file_name)
                    if get_choice_from_user():
                        if not is_dry_run:
                            os.remove(os.path.join(root, file_name))

                for dir_name in dirs:
                    print "Delete dir {path}?".format(path=dir_name)
                    if get_choice_from_user():
                        if not is_dry_run:
                            os.rmdir(os.path.join(root, dir_name))

            print "Delete dir {path}?".format(path=path)
            if get_choice_from_user():
                if os.listdir(path):
                    print "Dir not empty"
                    LOGGER.debug("Call recursive, because dir not empty")
                    delete_recursive_with_confirmation(
                        trash,
                        path,
                        is_preserve_root,
                        is_dry_run,
                        is_recursive
                    )

                else:
                    if not is_dry_run:
                        os.rmdir(path)

    else:
        print "Can't delete system file"
        LOGGER.error("Try to delete system dir %s", path)
        write_trash_logs(trash, "Try to delete system file --> {file_name}"
                         .format(file_name=path))


def remove_to_trash(trash,
                    files,
                    is_recursive=False,
                    is_interactive_input=False,
                    is_preserve_root=False,
                    is_dry_run=False):
    """
    Move files or dirs to local trash
    :param trash: 
    :param files: paths to files
    :param is_interactive_input: atribute -i - prompt before every removal
    :param is_recursive: atribute -r, which call recursuve delete 
    :param is_preserve_root: atribute --preserve-root - allow to delete sys dir and files
    :param is_dry_run: atribute --dry-run - print what will be do
    """
    LOGGER.debug("Call remove_to_trash")
    try:
        for path_to_file in files:
            file_name = os.path.abspath(path_to_file)
            if is_interactive_input and is_exist(file_name):
                if can_remove(trash, file_name, is_preserve_root, is_recursive):

                    if os.path.islink(file_name):
                        print "Use -p to delete link"

                    print "Move file to trash{}?".format(file_name)
                    if get_choice_from_user():
                        if not is_dry_run:
                            trash.move_to_trash([file_name])
                            write_trash_logs(trash, "Remove file to trash --> {file_name}".
                                             format(file_name=file_name))
                else:
                    LOGGER.error("Not enough space to delete %s", file_name)
                    write_trash_logs(trash, "Error while deleting file --> {file_name}".
                                     format(file_name=file_name))
                    print "Can't delete file --> {file_name}".format(file_name=file_name)

            elif is_exist(file_name):
                if can_remove(trash, file_name, is_preserve_root, is_recursive):
                    if not is_dry_run:
                        trash.move_to_trash([file_name])
                        write_trash_logs(trash, "Remove file to trash --> {file_name}".
                                         format(file_name=file_name))

                else:
                    LOGGER.error("Can't remove file %s", file_name)
                    write_trash_logs(trash, "Error while deleting file --> {file_name}"
                                     .format(file_name=file_name))

    except IOError:
        raise Exception("Error while remove file")


def restore_files(trash, files):
    """
    Restore files or dirs from you local trash
    :param trash: 
    :param files: names of files
    """
    try:

        for file_name in files:
            result, is_success = trash.restore_file([file_name])
            if not is_success:
                print "File already exist, rewrite it?"
                if get_choice_from_user():
                    trash.restore_file(file_name, replace_file=True)

    except IOError:
        raise Exception("Error while restoring")
