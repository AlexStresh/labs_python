"""
Logger with parameters
"""


class LoggerWithParameters(object):
    _log = []
    func_name = False
    argg_name = False
    arrg_value = False
    result_value = False

    def __init__(self, func_name=True, argg_name=True, arrg_value=True, result_value=True):
        if func_name is not None:
            self.func_name = func_name
        if argg_name is not None:
            self.argg_name = argg_name
        if arrg_value is not None:
            self.argg_value = arrg_value
        if result_value is not None:
            self.result_value = result_value

    def __getattribute__(self, attrs):
        log = {}
        attr = super(LoggerWithParameters, self).__getattribute__(attrs)
        if callable(attr):
            def logger(*args, **kwargs):
                result = attr(*args, **kwargs)

                if self.argg_name:
                    log.update({"Func Name": attrs})
                if self.argg_name:
                    arrg_name = [val for val in kwargs.keys()]
                    log.update({"Arg Name": arrg_name})
                if self.argg_value:
                    arrg_name = [val for val in args]
                    log.update({"Arg Value": arrg_name})
                if self.result_value:
                    log.update({"Result": result})
                self._log.append(log)
                return result
            return logger
        else:
            return attr

    def __str__(self):
        output_str = ''
        for elem in self._log:
            for key, value in elem.items():
                param_name, param_value = key, value
                output_str += str(param_name) + ': '
                param_str = ''
                if hasattr(param_value, '__iter__'):
                    for value in param_value:
                        param_str += str(value)
                else:
                    param_str = str(param_value)
                output_str += param_str + '\n'
        return output_str

    def test_method(self, param):
        return param


if __name__ == "__main__":
    test_obj = LoggerWithParameters(True, True, True, True)
    test_obj.test_method('123')
    print test_obj.__str__()
