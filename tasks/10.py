"""
Metaclass that take arg from text
"""
import os

path_to_file = '10'


def meta_decorator(cls):
    def wrapper(filename):
        cls.file = filename
        return cls
    return wrapper


@meta_decorator
class MetaAddArgs(type):

     def __new__(cls, name, bases, dct):
         path = os.path.abspath(cls.file)
         delattr(cls, 'file')
         with open(path, 'r') as args_file:
             strings = args_file.readlines()
         args_dict = {}
         for string in strings:
             args = string.strip().replace('\n', '').split(':')
             args_dict.update({args[0].strip(): args[1].strip()})
         dct.update(args_dict)

         return type.__new__(cls, name, bases, dct)


class Simple(object):
    __metaclass__ = MetaAddArgs('10')


if __name__ == "__main__":
    # path_to_file = raw_input()
    simple = Simple()
    print Simple.creator
