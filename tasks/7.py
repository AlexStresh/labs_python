"""
opp for n-vector 
"""

import os


class NVector(object):

    def __init__(self, dimension=None, values=None, file=None):
        if file is not None:
            path_to_file = os.path.abspath(file)
            with open(path_to_file, 'r') as file_input:
                string = file_input.readlines()
            dim = int(string[0].strip().replace('\n', ''))
            val = [int(val.strip().replace('\n', '')) for val in string[1].split(' ')]
            self.dimension = dim
            self.values = val
        else:
            self.dimension = dimension
            self.values = values

    def sum(self,  second_vector):
        if not isinstance(self, NVector) and (second_vector, NVector):
            return "error type"
        if self.dimension == second_vector.dimension:
            count = self.dimension
            for val in xrange(0, count):
                self.values[val] = self.values[val] + second_vector.values[val]
            return self.values
        else:
            print "not in one dimension"

    def diff(self, second_vector):
        if not isinstance(self, NVector) and not isinstance(second_vector, NVector):
            return "error type"
        if self.dimension == second_vector.dimension:
            count = self.dimension
            for val in xrange(0, count):
                self.values[val] = self.values[val] - second_vector.values[val]
            return self.values
        else:
            print "not in one dimension"

    def mul_by_const(self, const):
        if not isinstance(self, NVector) and (second_vector , NVector):
            return "error type"
        if self.dimension == second_vector.dimension:
            count = self.dimension
            for val in xrange( 0 , count ):
                self.values[ val ] = self.values[ val ] * const
            return self.values
        else:
            print "not in one dimension"


if __name__ == "__main__":
    first_vector = NVector(file='for_7')
    second_vector = NVector(file='for_7')
    print first_vector.sum(second_vector)
    print first_vector.diff(second_vector)
    print first_vector.mul_by_const(10)
