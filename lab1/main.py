#!/usr/bin/env python2
# -*-coding:utf-8-*-
""" Simple script for first python lab

"""


import sys


def main():
    """Example function that types format

    """
    print "Hello World"
    print "My name {name}".format(name=sys.argv[0])


if __name__ == '__main__':
    main()
