"""
decorator for func
"""


def decorator(func):
    def wrapped(*args, **kwargs):
        with open('11', 'r') as cached_file:
            calls = cached_file.readlines()

        args_dict = {}
        for string in calls:
            argument = string.strip().replace('\n', '').split(':')
            args_dict.update({str(argument[0].strip()): argument[1].strip()})

        string = ''

        if args:
            for arg in args:
                string += str(arg)
        if kwargs:
            print 'ya'
            for key, value in kwargs.items():
                string += str(key) + str(value)

        string += func.__name__

        if string in args_dict:
            print 'Ya! Already taken such args'
            return args_dict[string]

        else:
            print 'No! New args. Nice!'
            result = func(*args, **kwargs)
            args_dict.update({string: result})
            with open('11', 'w+') as cached_file:
                cached_file.truncate()
                for key, value in args_dict.items():
                    cached_file.write('{key} : {value}\n'.format(key=key, value=value))
            return result

    return wrapped


@decorator
def simple_sum(a, b):
    return a + b


if __name__ == '__main__':
    print simple_sum(1, 2)
