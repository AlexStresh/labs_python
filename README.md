# Labs python

# Lab №2 "SmartRm" 

UNIX OS utilite providing removing/restoring your files and folders, like trashbin in Windows. 
Developed on Python 2.7



## Installation guide


Download or clone this repository: 
``` bash
git clone https://AlexStresh@bitbucket.org/AlexStresh/labs_python.git
```
go to dir 'Lab2', find 'setup.py' file and install it:
``` bash
sudo python setup.py install
```



## Usage guide:


To access removing facilities, run the next command in the terminal:

``` bash
smrm [arguments] [path_to_file]
```

If run without arguments - can remove only files to trash 

To remove files permanently use *-p(--permanent)* arguments:
``` bash
smrm -p [path_to_file]
```

To remove not empty directories *-r* argument:
``` bash
smrm -r [path_to_file]
```

To watch how the application works without any changes, use *--dryrun* argument:
``` bash
smrm --dryrun [path_to_file]
```


If you want to control process of deleting and see deleting file use *-i* argument:
``` bash
smrm -i [path_to_file]
```


If you want to change trash settings(such as: path to trash, trash max size and other)
use *--create_trash* argument:
``` bash
smrm --create_trash [path_to_trash]
```

Than change values you want use nedded arguments:
``` bash
smrm --max_count [MAX_COUNT]
```
``` bash
smrm --max_size [MAX_SIZE]
```
``` bash
smrm --max_hours [MAX_HOURS]
```

You can look on your config file like that:
``` bash
smrm --config
```

If you want to clean your trash use *--clean* argument:
``` bash
smrm --clean
```


## Moodules description

1. **-smart_rm.py**

    Represents application's entry point. The place where all the main methods are called.

2. **-trash.py**

    The module provides main logic for trash as main class Trash and methods wich work with 
	object Trash.
	
3. **-utils.py**

    The module provides some function for console such as print config file on the screen,
	write trash logs and trash history.

4. **-setup.py**

    Specifies such information as version, software name, description and entry points for the
    application after installation. Hence the name.

5. **-shell_component**

    The module provides logic that connects trash.py and user shell (print messages in console,
	get user choice and some other).




# Lab №3 "Trashes Controler"

Local web app providing removing/restoring opeartions with file storage support.

Developed on python 2.7 & Django 1.11.4

## Usage guide:

### To use this app you must have nginx and gunicorn.

To install this piece of software, run:

``` bash
pip install gunicorn
pip install nginx
```

To run server, type in webrm project folder:

``` bash
service nginx start
gunicorn trashes_control.wsgi
```

You can see your web if type

``` bash
127.0.0.1 
```
in your web-browser adress input.


If you need to know use this web, you can visit page "Help" when start it.
