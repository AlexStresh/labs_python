import threading
import time
import multiprocessing


def set_path(list_str):
    res = []
    if type(list_str) != list:
        raise TypeError("Must be list")
    set_str = set(list_str)
    list_str = [elem for elem in set_str]
    list_str.sort()
    res.append(list_str.pop(0))
    for str in list_str:
        if str.startswith(res[0]):
            continue
        else:
            res.insert(0, str)
    return res


def multiproc(num):
    for _ in xrange(5):
        print num
        print multiprocessing.current_process()
        time.sleep(0.5)


if __name__ == "__main__":
    pool = multiprocessing.Pool(processes=4)
    for i in xrange(10):
        pool.apply_async(multiproc, (i,))
        # pool.apply(multiproc, (i,))
    pool.close()
    pool.join()
