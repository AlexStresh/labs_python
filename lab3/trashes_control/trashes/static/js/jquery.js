$(document).ready(function () {

    let get_current_dir_content = function(that) {
        $(that).val("-");
        $(that).text("-");

        $.ajax({
            url: `/get_files/${$(that).attr("name")}/`,
        })

        .done(function(data) {
                btn_name = $(that).attr("name");
                tag = btn_name.split(/[/() ,_]/).join("");
                console.log(data);
                let markup = `<div class=subdirs id='${tag}'>`;
                let content = JSON.parse(data)[0];

                for (let key in content) {
                    let path = key.split('/');
                    path = path[path.length-1];

                    if (content[key]) {
                        markup +=
                            `<div>
                                <button class=expander type=button name='${key}' value=+>+</button> 
                                <input type=checkbox value='${key}' name='${key}'>
                                <label for='${key}'>${path}</label>
                            </div>`;
                    }
                    else {
                        markup +=
                            `<div>
                                <input type=checkbox value='${key}' name='${key}'>
                                <label for='${key}'>${path}</label>
                            </div>`;
                    }
                }
                markup += "</div>";

                $("label").each(function() {
                    if ($(this).attr("for") == btn_name) {
                        $(this).after(markup);
                        return false;
                    }
                });
            });
    }

    let clickHandler = function(event) {
        if ($(this).val() == "+") {
            get_current_dir_content(this);
        }
    }

    $(".filetree").on("click", "button", clickHandler);

});


