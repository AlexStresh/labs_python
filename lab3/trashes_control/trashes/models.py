# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from multiprocessing import RLock

from validators import validate_positive_numb
from bin.trash import Trash as ClassTrash, is_valid_trash
from bin.shell_component import delete_by_date_policy

lock = RLock()


class Trash(models.Model):
    """
    class Trash - representation of Trash model that create
                  and save model to db.
    """
    path_to_trash = models.CharField(max_length=256, unique=True)
    trash_name = models.CharField(max_length=256)
    trash_max_size = models.PositiveIntegerField(default=256,
                                                 validators=[validate_positive_numb])
    trash_max_count = models.PositiveIntegerField(default=256,
                                                  validators=[validate_positive_numb])
    trash_max_hours = models.PositiveIntegerField(default=24,
                                                  validators=[validate_positive_numb])
    add_date = models.DateTimeField(default=timezone.now())
    creator = models.ForeignKey(User)
    is_valid = models.BooleanField(default=False)
    date_politic = models.BooleanField(default=False)
    dry_run = models.BooleanField(default=False)

    def __unicode__(self):
        return self.trash_name

    def create_trashes(self):
        """
        Create trash with settings from from model
        :return: object trash
        """
        trash = ClassTrash.create_trash(self.path_to_trash)
        return trash

    def get_trash_content(self):
        """
        Get files from trash
        :return: return trash content
        """
        trash = self.create_trashes()
        return trash.get_trash_content()

    def change_trash_settings(self):
        """
        Change trash settings using data from db
        :return: 
        """
        trash = self.create_trashes()
        trash.change_max_size(self.trash_max_size)
        trash.change_max_count(self.trash_max_count)
        trash.change_max_hours(self.trash_max_hours)

    def clean_trash(self):
        """
        :return: None 
        """
        trash = self.create_trashes()
        trash.clean_trash()

    def is_valid_trash(self):
        """
        Check if dir contains .config file with
        settings 
        :return: 
        """
        trash = self.create_trashes()
        return is_valid_trash(trash)

    def clean_trash_by_date_policy(self, user):
        """
        Remove file from trash by date policy using field
        max_hours from model
        :return: deleted by date policy files  
        """
        trash = self.create_trashes()
        files = delete_by_date_policy(trash)

        for file_name in files:
            Tasks.objects.create_task(
                trash=self,
                task=Tasks.TRASH_CLEAN_BY_DATE_POLITIC,
                files=file_name,
                status=Tasks.STATUS_COMPLETED,
                commit='Success',
                creator=user
            )

        return files


class TasksManager(models.Manager):
    """
    class TaskManager - class Manager that manage class Tasks,
                        create and save Tasks object to db.
    """
    def create_task(self, trash, task, files, status, commit, creator, dry_run=False):
        lock.acquire()
        task = self.create(trash=trash,
                           task=task,
                           files=files,
                           status=status,
                           commit=commit,
                           creator=creator,
                           dry_run=dry_run)
        lock.release()
        return task


class Tasks(models.Model):
    """
    class Tasks - representation of Tasks model that create
                  and save model to db.
    """
    TRASH_DELETE = 0
    TRASH_RESTORE_WITH_REWRITE = 1
    TRASH_CLEAN = 2
    TRASH_RESTORE_WITHOUT_REWRITE = 3
    TRASH_CLEAN_BY_DATE_POLITIC = 4

    STATUS_COMPLETED = 0
    STATUS_RUNNING = 1
    STATUS_PAINTING = 2

    STATUS_CHOICES = (
        (STATUS_COMPLETED, "COMPLETED"),
        (STATUS_RUNNING, "RUNNING"),
        (STATUS_PAINTING, "PAINTING"),
    )

    TASK_CHOICES = (
        (TRASH_DELETE, "DELETE"),
        (TRASH_RESTORE_WITH_REWRITE, "RESTORE WITH REWRITE"),
        (TRASH_RESTORE_WITHOUT_REWRITE, "RESTORE WITHOUT REWRITE"),
        (TRASH_CLEAN, "CLEAN"),
        (TRASH_CLEAN_BY_DATE_POLITIC, "CLEAN BY DATE POLITIC"),
    )

    creator = models.ForeignKey(User)
    status = models.IntegerField(choices=STATUS_CHOICES, default=STATUS_PAINTING)
    create_date = models.DateTimeField(default=timezone.now())
    trash = models.ForeignKey(Trash, on_delete=models.CASCADE,)
    task = models.IntegerField(choices=TASK_CHOICES)
    files = models.CharField(max_length=256)
    objects = TasksManager()
    commit = models.CharField(max_length=256, default="")
    dry_run = models.BooleanField(default=False)

    def run_task(self):
        """
        Create and complete task from task manager
        :return: None
        """

        lock.acquire()
        results = {}

        trash_model = self.trash
        trash = trash_model.create_trashes()
        trash.dry_run = self.dry_run
        file_name = self.files
        task = self.task

        self.status = Tasks.STATUS_RUNNING
        self.save()

        if task == Tasks.TRASH_DELETE:
            results = trash.move_to_trash_thread([file_name])
        elif task == Tasks.TRASH_RESTORE_WITH_REWRITE:
            results = trash.restore_file_thread([file_name], replace_file=True)
        elif task == Tasks.TRASH_RESTORE_WITHOUT_REWRITE:
            results = trash.restore_file_thread([file_name], replace_file=False)

        self.status = Tasks.STATUS_COMPLETED
        for file_name, result in results.items():
            self.files = file_name
            self.commit = result
        self.save()
        lock.release()
