from django.conf.urls import url , include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import views

handler404 = 'trashes.views.call_bad_request'

urlpatterns = [
    url(r'^$', views.open_start_page),
    url(r'^add', views.add_trash, name='add_trash'),

    url(r'^trash/(?P<pk>[0-9]+)/$', views.open_trash_detail, name='trash_detail'),
    url(r'^trash/(?P<pk>[0-9]+)/edit/$', views.edit_trash, name='trash_edit'),
    url(r'^trash/(?P<pk>[0-9]+)/remove$', views.delete_trash, name='delete_trash'),
    url(r'^trash/(?P<pk>[0-9]+)/delete$', views.delete, name='delete'),
    url(r'^trash/(?P<pk>[0-9]+)/add-restore-task$', views.add_restore_task, name='add_restore_task'),
    url(r'^trash/(?P<pk>[0-9]+)/add-delete-task$', views.add_delete_task, name='add_delete_task'),
    url(r'^trash/(?P<pk>[0-9]+)/clean$', views.clean_trash, name='clean_trash'),
    url(r'^trash/(?P<pk>[0-9]+)/restore$', views.restore_files, name='restore_files'),

    url(r'^task/(?P<pk>[0-9]+)/$', views.delete_task, name='delete_task'),
    url(r'^task/delete-all/$', views.delete_all_task, name='delete_all_task'),
    url(r'^task/run-all/$', views.run_all_task, name='run_all_task'),
    url(r'^task/(?P<pk>[0-9]+)/run/$', views.run_task, name='run_task'),

    url(r'get_files/(?P<cur_dir>.+)/$', views.get_files, name='get_files'),
    url(r'^delete/', views.delete, name='delete'),

    url(r'^about/', views.open_about_page, name='open_about_page'),
    url(r'^help/', views.render_help_page, name='open_help_page'),

    url(r'^cabinet/', views.render_user_cabinet, name='render_user_cabinet'),
    url(r'^user/password/', views.change_password, name='change_password'),

    url(r'^login/', views.login_user, name='login_user'),
    url(r'logout', views.logout_user),
    url(r'^register/', views.register_user),

    url(r'^i18n/', include('django.conf.urls.i18n')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
