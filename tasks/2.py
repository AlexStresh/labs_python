"""
Quick Sort/Merge Sort
"""


def quick_sort(numbers):
    less = []
    equals = []
    more = []

    if len(numbers) > 1:
        pivot = numbers[0]
        for number in numbers:
            if number < pivot:
                less.append(number)
            elif number > pivot:
                more.append(number)
            else:
                equals.append(number)
        return quick_sort(less) + equals + quick_sort(more)
    else:
        return numbers


def merge_sort(numbers):

    if len(numbers) > 1:
        mid = int(len(numbers)/2)
        left = numbers[0:mid]
        right = numbers[mid:]

        merge_sort(left)
        merge_sort(right)

        i = 0
        j = 0
        k = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                numbers[k] = left[i]
                i += 1
            else:
                numbers[k] = right[j]
                j += 1
            k += 1
        while i < len(left):
            numbers[k] = left[i]
            i += 1
            k += 1
        while j < len(right):
            numbers[k] = right[j]
            j += 1
            k += 1

        return numbers


def start_sort():
    print "Enter string of numbers to sort"
    # numbers = raw_input()
    numbs = "5 4 3 2 1"
    numbers = [int(numb) for numb in numbs.strip().split(' ')]
    print merge_sort(numbers)
    print quick_sort([5, 4, 3, 2, 1])


if __name__ == "__main__":
    start_sort()
