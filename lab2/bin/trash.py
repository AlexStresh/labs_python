# encoding: utf-8
# module trash
"""
This module provides work with local trash on your computer:
create and use trash for their own purposes.

Before using trash, you need to create it with settings:
    - path_to_trash --> local path to your trash on computer
    - max_size --> max trash size
    - max_count --> max trash count
    - max_hours --> the max time that the trash will store files
    - size_policy --> activate/disable delete by size policy
    - date_policy --> activate/disable delete by date policy
    
1) To create trash you use class Trash like this:
- Trash.create_trash(path_to_trash) --> this method call constructor __init__
 with special arguments.
   For example:
   1. trash = Trash.create_trash(path_to_trash) --> it will be create create trash
    with your path_to_trash and call __init__ with argument(path_to_trash=path_to_trash).
    But if this trash already exists(there is a config file in trash),
    this method call __init__ with arguments(config=config) and create trash with settings 
    that already exists.
   2. trash=Trash.create_trash() --> create basic trash with _BASIC_CONFIG_DICT(basic settings)
- Or you can call constructor __init__()
    For example:
     trash = Trash.__init__(argv) - it will create trash with your arguments(settings)
     
2) Then you can use your trash:
    1. trash.move_to_trash(files) --> move files to your trash, you can restore it
    2. trash.restore_file(files) --> restore files from your trash.
    3. trash.get_trash_content() --> get files in your trash that you can restore
    4. trash.clean_trash() --> clean your trash (delete all files from trash, you can't restore it)
    5. trash.change_max_size(max_size) --> change trash max size
    6. trash.change_max_count(max_count) --> change trash max count
    7. trash.change_max_hours(max_hours) --> change trash max hours
    8. trash.change_date_policy() --> change date policy status(activate/disable)
    9. trash.change_size_policy() --> change size policy status(activate/disable)

3) You can also use methods which provide advanced logic of working with your trash:
    1. is_exist(path) --> check if file or dir is exist
    2. get_count(path) --> return dir or file count
    3. get_file_size(path) --> return dir or file size
    4. can_remove(path) --> check file is can remove
    5. delete_recursive(path) --> delete file or dir permanently(you can't restore it)
    6. is_valid_trash(path) --> check is trash already exist
    7. get_info_from_config(path) --> return dict with trash settings
    8. print_file(path) --> print file on the screen(use pydoc)

4) Please, don't use methods which begin with _method
    For example: _set_last_trash() or _is_size_ok()
    This methods use in internal module's logic
"""
import sys
import os
import pwd
import json
import pydoc
import threading
import uuid
import time

from datetime import datetime
from multiprocessing.pool import ThreadPool
from multiprocessing import cpu_count

LOCK = threading.RLock()
delete_files = {}
restore_files = {}
BYTE_TO_KB = 1024
_BASIC_CONFIG_DICT = {
    "path_to_trash": "{}".format(os.path.join(os.path.expanduser("~"), "trash")),
    "silent_mode": "False",
    "max_size": "{}".format(
        os.statvfs(os.path.expanduser("~")).f_bavail *
        os.statvfs(os.path.expanduser("~")).f_bsize / BYTE_TO_KB / 2
    ),
    "max_count": "100",
    "max_hours": "3",
    "size_policy": "False",
    "date_policy": "True"
}


def collect_delete_results(result):
    delete_files.update(result)


def collect_restore_results(result):
    result = result[0]
    restore_files.update(result)


class Trash(object):
    @staticmethod
    def create_trash(path_to_trash=None):
        """
        Create local trash by path
        :param path_to_trash: path to self.trash 
        :return: True and path to you trash - if you can create trash
                 False and None - if you can't create trash
        """
        if path_to_trash is None:
            return Trash.create_trash(_BASIC_CONFIG_DICT["path_to_trash"])

        if not os.path.isdir(path_to_trash):
            os.mkdir(path_to_trash)

            with open(os.path.join(path_to_trash, ".info_json"), "w+") as info:
                info.write("[]")

            config = _BASIC_CONFIG_DICT
            config["path_to_trash"] = path_to_trash
            path_to_config = os.path.join(path_to_trash, ".config")
            create_config(path_to_config, config)
            trash = Trash(config=config)
            return trash

        elif is_valid_trash(path_to_trash):
            path_to_config = os.path.join(path_to_trash, ".config")
            config = get_info_from_config(path_to_config)
            trash = Trash(config=config)
            return trash

        else:
            with open(os.path.join(path_to_trash, ".info_json"), "w+") as info:
                info.write("[]")

            config = _BASIC_CONFIG_DICT
            config["path_to_trash"] = path_to_trash
            path_to_config = os.path.join(path_to_trash, ".config")
            create_config(path_to_config, config)
            trash = Trash(config=config)
            return trash

    def __init__(self,
                 path_to_trash=None,
                 silent_mode=None,
                 max_size=None,
                 max_count=None,
                 max_hours=None,
                 size_policy=None,
                 date_policy=None,
                 config=None,
                 dry_run=False):

        self.path_to_trash = path_to_trash
        self.silent_mode = silent_mode
        self.max_size = max_size
        self.max_count = max_count
        self.max_hours = max_hours
        self.size_policy = size_policy
        self.date_policy = date_policy
        self.dry_run = dry_run

        if self.path_to_trash is None:
            self.path_to_trash = _BASIC_CONFIG_DICT["path_to_trash"]

        if self.silent_mode is None:
            self.silent_mode = bool(_BASIC_CONFIG_DICT["silent_mode"])

        if self.max_size is None:
            self.max_size = int(_BASIC_CONFIG_DICT["max_size"])

        if self.max_count is None:
            self.max_count = int(_BASIC_CONFIG_DICT["max_count"])

        if self.max_hours is None:
            self.max_hours = int(_BASIC_CONFIG_DICT["max_hours"])

        if self.size_policy is None:
            self.size_policy = bool(_BASIC_CONFIG_DICT["size_policy"])

        if self.date_policy is None:
            self.date_policy = bool(_BASIC_CONFIG_DICT["date_policy"])

        if config is not None:
            self.path_to_trash = config["path_to_trash"]
            self.silent_mode = bool(config["silent_mode"])
            self.max_size = int(config["max_size"])
            self.max_count = int(config["max_count"])
            self.max_hours = int(config["max_hours"])
            self.size_policy = bool(config["size_policy"])
            self.date_policy = bool(config["date_policy"])

    def move_to_trash_thread(self, files):
        """
        Move file to trash using processing
        :param files: list of files to delete
        :return: dict with info about deleting
        """
        delete_files.clear()
        thread_pool = ThreadPool(processes=cpu_count())
        for file_name in files:
            thread_pool.apply_async(self.move_to_trash,
                                    args=([file_name], ),
                                    callback=collect_delete_results)
            time.sleep(0.01)

        thread_pool.close()
        thread_pool.join()
        return delete_files

    def restore_file_thread(self, files, replace_file=False):
        """
        Restore files from trash using processing
        :param files: files to restore 
        :param replace_file: bool value
        :return: dict with info about restoring
        """
        restore_files.clear()
        thread_pool = ThreadPool(processes=cpu_count())
        for file_name in files:
            thread_pool.apply_async(self.restore_file,
                                    args=([file_name], replace_file, ),
                                    callback=collect_restore_results)
            time.sleep(0.01)

        thread_pool.close()
        thread_pool.join()
        return restore_files

    def move_to_trash(self, files):
        """
        Trash.move_to_trash --> Remove file or dir to trash,
                          you can restore it using restore_file()
        :param files: path to file which we delete 
        """
        files_delete = {}
        for file_name in files:
            file_name = os.path.abspath(file_name)
            if is_exist(file_name) and not _is_sys_dir(file_name) and not self.dry_run \
                    and can_remove(self,
                                   file_name,
                                   is_preserve_root=False,
                                   is_recursive=True):

                uni_uuid = str(uuid.uuid4())
                uuid_name = os.path.join(self.path_to_trash, uni_uuid)
                os.rename(file_name, uuid_name)
                LOCK.acquire()
                _update_info_json(self.path_to_trash, file_name, uni_uuid)
                LOCK.release()
                files_delete.update({file_name: "Success"})

            else:
                error_messages = update_error_messages(self,
                                                       file_name,
                                                       is_recursive=True)
                error_messages = "".join(error_messages)
                files_delete.update({file_name: error_messages})

        return files_delete

    def restore_file(self, files, replace_file=False):
        """
        Restore file or dir from your local trash
        :param replace_file: 
        :param files: file name to restore
        :return: None and True - if restore successfully
                 path to file and False - if file or dir already exists
        """
        _is_settings_created()
        info = _load_info_json(self.path_to_trash)
        restored_files = {}
        for file_name in files:
            for file_info in info:
                if file_info["name"] == file_name:
                    uuid_name = os.path.join(self.path_to_trash, file_info["UUID"])
                    file_name = os.path.join(file_info["file_path"], file_info["name"])

                    if self.dry_run:
                        restored_files.update({file_name: "Dry-Run mode is active"})
                        return restored_files, False

                    if not is_exist(file_name):
                        LOCK.acquire()
                        os.renames(uuid_name, file_name)
                        info.remove(file_info)
                        _write_file_info_json(self.path_to_trash, info)
                        restored_files.update({file_name: "Success"})
                        LOCK.release()

                    elif is_exist(file_name) and replace_file:
                        LOCK.acquire()
                        delete_recursive(file_name)
                        os.renames(uuid_name, file_name)
                        info.remove(file_info)
                        _write_file_info_json(self.path_to_trash, info)
                        restored_files.update({file_name: "Success"})
                        LOCK.release()

                    else:
                        restored_files.update({file_name: "File already exist"})
                        return restored_files, False

        return restored_files, True

    def get_trash_content(self):
        """
        Return list of object in trash
        """
        trash_files = {}
        info = _load_info_json(self.path_to_trash)

        for field in info:
            fake_name = os.path.join(self.path_to_trash, field["UUID"])
            if not is_exist(fake_name):
                info.remove(field)
                _write_file_info_json(self.path_to_trash, info)

            else:
                trash_files.update({field["name"]: field["file_path"]})

        return trash_files

    def clean_trash(self):
        """
        Clean your local trash, get path to your trash from
        config file
        """
        _is_settings_created()
        try:

            if self.dry_run:
                return None

            for file_name in os.listdir(self.path_to_trash):
                if (file_name == ".info_json") or (file_name == ".trash_logs") or (file_name == ".config"):
                    continue

                path_file = os.path.join(self.path_to_trash, file_name)
                delete_recursive(path_file)

            with open(os.path.join(self.path_to_trash, ".info_json"), "w") as info:
                info.truncate()
                info.write("[]")

            with(open(os.path.join(self.path_to_trash, ".trash_logs"), "w")) as logs:
                logs.truncate()

        except IOError:
            sys.exit(1)

    def __change_trash_settings(self, config):
        """
        Change settings in trash config file 
        :param config: new config file in dir
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        with (open(path_to_config, "w")) as config_file:
            for key in config.iterkeys():
                setting = "{key}:{value}\n".format(key=key, value=config[key])
                config_file.write(setting)

    def change_silent_mode(self):
        """
        Activate/disable silent mode
        :return currant silent mode status
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)
        current_status = config["silent_mode"]

        if current_status == "True":
            self.silent_mode = False

        else:
            self.silent_mode = True

        config["silent_mode"] = self.silent_mode
        self.__change_trash_settings(config)

        return self.silent_mode

    def change_date_policy(self):
        """
        Activate/disable delete by date policy
        :return current date policy status
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)

        current_status = config["date_policy"]
        if current_status == "True":
            self.date_policy = False

        else:
            self.date_policy = True

        config["date_policy"] = self.date_policy
        self.__change_trash_settings(config)

        return self.date_policy

    def change_size_policy(self):
        """
        Activate/disable delete by size policy
        :return current size policy status
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)

        current_status = config["size_policy"]
        if current_status == "True":
            self.size_policy = False

        else:
            self.size_policy = True

        config["size_policy"] = self.size_policy
        self.__change_trash_settings(config)

        return self.size_policy

    def change_max_hours(self, max_hours):
        """
        Change Trash field max_hours
        :param max_hours: max hours that file can restore 
        :return current max_hours
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)

        try:
            if int(max_hours) > 0:
                self.max_hours = max_hours
                config["max_hours"] = max_hours
                self.__change_trash_settings(config)

                return self.max_hours

            return False

        except ValueError:
            raise Exception("Incorrect input")

    def change_max_count(self, max_count):
        """
        Change Trash field max_count
        :param max_count: max Trash count 
        :return: current max_count
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)

        try:
            if int(max_count) > 0:
                self.max_count = max_count
                config["max_count"] = max_count
                self.__change_trash_settings(config)

                return self.max_count

            return False

        except ValueError:
            raise Exception("Incorrect input")

    def change_max_size(self, max_size):
        """
        Change Trash field max_size
        :param max_size: max Trash size 
        :return: current max_count
        """
        path_to_config = os.path.join(self.path_to_trash, ".config")
        config = get_info_from_config(path_to_config)

        try:
            size_temp = os.statvfs(self.path_to_trash)
            available_size = size_temp.f_bavail * size_temp.f_bsize / BYTE_TO_KB / 2
            if 0 < int(max_size) <= available_size:
                self.max_size = max_size
                config["max_size"] = max_size
                self.__change_trash_settings(config)

                return True

            return False

        except ValueError:
            raise Exception("Incorrect input")


def set_last_used_trash(path_to_trash):
    """
    Write in .last_trash file last used Trash
    :param path_to_trash: path to last used Trash
    """
    _is_settings_created()
    path_to_last_trash = os.path.join(
        os.path.expanduser("~"),
        ".config/smart_rm/.last_trash")

    with open(path_to_last_trash, "w") as last_trash:
        last_trash.truncate()
        last_trash.write(path_to_trash)


def get_last_used_trash():
    """
    Read from .last_trash file last used Trash 
    :return: path to last used Trash or None, if file is empty
    """
    _is_settings_created()
    path_to_last_trash = os.path.join(
        os.path.expanduser("~"),
        ".config/smart_rm/.last_trash")

    if not is_exist(path_to_last_trash) or os.path.getsize(path_to_last_trash) == 0:
        return None

    with open(path_to_last_trash, "r") as last_trash:
        path_to_trash = last_trash.readline().rstrip("\n")
        path_to_trash = os.path.abspath(path_to_trash)

    return path_to_trash


def init_program_settings():
    """
    Initialize program settings and start program:
        - create dir .config
        - create dir smart_rm
        - create file .last_trash
        - create basic Trash
    """
    setup_path = os.path.join(os.path.expanduser("~"), ".config")
    path_to_smrm = os.path.join(setup_path, "smart_rm")
    path_to_last_trash = os.path.join(setup_path, "smart_rm/.last_trash")

    if not os.path.isdir(setup_path):
        os.mkdir(setup_path)

    if not os.path.isdir(path_to_smrm):
        os.mkdir(path_to_smrm)
        Trash.create_trash()

    if not path_to_last_trash:
        set_last_used_trash(_BASIC_CONFIG_DICT["path_to_trash"])


def _is_settings_created():
    """
    Check if the program settings are created
    :return: True - if settings created, else call init_program_settings()
    """
    path_to_smrm = os.path.join(os.path.expanduser("~"), ".config/smart_rm")
    path_to_last_trash = os.path.join(path_to_smrm, ".last_trash")

    if not os.path.isdir(path_to_smrm) or not is_exist(path_to_last_trash):
        init_program_settings()

    else:
        return True


def is_exist(path):
    """
    Check is exist file or dir by path
    :param path: path to file or dir
    :return: True - if file or dir exist
             False - if file or dir doesn't exist
    """
    path_to_file = os.path.abspath(path)
    return os.path.isfile(path_to_file) or os.path.isdir(path_to_file)


def get_count(path):
    """
    Get number of files by path
    :return: number of files
    """
    path_to_file = os.path.abspath(path)
    try:

        if is_exist(path_to_file):
            if os.path.isfile(path_to_file) or os.path.islink(path_to_file):

                return 1

            count = 0
            if is_exist(path_to_file):
                for root, dirs, files in os.walk(path_to_file):
                    for _ in files:
                        count += 1
                    for _ in dirs:
                        count += 1

                return count

        else:
            sys.exit(1)

    except IOError:
        raise Exception("Error while get file count")


def get_file_size(path):
    """
    Get size of files or dirs in Mb
    :return: size of files or dirs in Mb
    """
    path_to_file = os.path.abspath(path)

    if os.path.isfile(path_to_file):
        return os.path.getsize(path_to_file)/BYTE_TO_KB

    if os.path.islink(path_to_file):
        return 1

    total_size = 0
    if os.path.isdir(path_to_file):
        for root, _, files in os.walk(path_to_file):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                total_size += os.path.getsize(file_path)

        return total_size

    return None


def _is_size_ok(trash, path):
    """
    Compare file size with trash available size 
    :type trash:
    :param path: path to file or dir
    :return: True - if you can delete file
             False - if you can't
    """
    _is_settings_created()
    path_to_file = os.path.abspath(path)
    size = os.statvfs(str(trash.path_to_trash))
    available_size = size.f_bsize * size.f_bavail

    if get_file_size(trash.path_to_trash) + get_file_size(path_to_file) > available_size:
        return False

    else:
        return True


def _is_count_ok(trash, path):
    """
    Compare files count with trash available count 
    :type trash:
    :param path: path to file or dir
    :return: True - if you can delete file
             False - if you can't
    """
    _is_settings_created()
    path_to_file = os.path.abspath(path)
    max_count = int(trash.max_count)
    if get_count(path_to_file) + get_count(trash.path_to_trash) > max_count:
        return False

    else:
        return True


def _is_sys_dir(path):
    """
    Check: is file or dir is system
    :param path: absolute path to file or dir
    :return:True - if file or dir is system
            False - if not system
    """
    checked_path = os.path.abspath(path)

    if not checked_path.startswith(os.path.expanduser('~')):
        return True

    else:
        return False


def _is_contain_root(path):
    """
    Check: contain root file in dir
    :param path: path to file
    :return: True - if contains
            False - if not
    """
    path_to_file = os.path.abspath(path)
    is_root = False

    if os.path.isfile(path_to_file):
        if pwd.getpwuid(os.stat(path_to_file).st_uid).pw_name == "root":
            is_root = True

    elif os.path.isdir(path_to_file):
        if pwd.getpwuid(os.stat(path_to_file).st_uid).pw_name == "root":
            is_root = True

        for root, dirs, files in os.walk(path_to_file, topdown=False):
            for file_name in files:
                path_to_file = os.path.join(root, file_name)
                if pwd.getpwuid(os.stat(path_to_file).st_uid).pw_name == "root":
                    is_root = True

            for dir_name in dirs:
                path_to_dir = os.path.join(root, dir_name)
                if pwd.getpwuid(os.stat(path_to_dir).st_uid).pw_name == "root":
                    is_root = True

    return is_root


def update_error_messages(trash, path_to_file, is_recursive):
    """
    Indetify error why file can't be remove
    :param trash: onject Trash
    :param is_recursive: argument -r(--recursive) --> call recursive delete
    :param path_to_file: path to file or dirs 
    :return: True - if you can delete file
             False - if you can't
    """
    error_messages = []
    path_to_file = os.path.abspath(path_to_file)
    is_root = _is_contain_root(path_to_file)

    if trash.path_to_trash == path_to_file:
        error_messages.append("Can't delete file from trash\n")

    if is_root and (os.getuid() != 0):
        if os.path.isfile(path_to_file):
            error_messages.append("Can't delete files that create root\n")
        else:
            error_messages.append("Can't delete dir that have root file\n")

    if not _is_size_ok(trash, path_to_file) or not _is_count_ok(trash, path_to_file):
        error_messages.append("Can't delete file - trash full\n")

    if _is_sys_dir(path_to_file):
        error_messages.append("Can't delete file - system dir\n")

    if os.path.isdir(path_to_file) and not is_recursive:
        error_messages.append("Can't delete dir - use argument -r\n")

    if trash.dry_run:
        error_messages.append("Dry-Run Mode is active")

    return error_messages


def can_remove(trash, path_to_file, is_preserve_root, is_recursive):
    """
    check is can remove file to trash
    :param trash: 
    :param is_recursive: argument -r(--recursive) --> call recursive delete
    :param path_to_file: path to file or dirs 
    :param is_preserve_root: argument --preserve-root --> allow to delete sys dirs and files
    :return: True - if you can delete file
             False - if you can't
    """
    path_to_file = os.path.abspath(path_to_file)
    is_root = _is_contain_root(path_to_file)

    if trash.path_to_trash == path_to_file:
        return False

    if is_root and (os.getuid() != 0):
        return False

    if _is_size_ok(trash, path_to_file) \
            and _is_count_ok(trash, path_to_file) \
            and (not _is_sys_dir(path_to_file) or is_preserve_root) \
            and (os.path.isfile(path_to_file)
                 or (os.path.isdir(path_to_file) and is_recursive)):
        return True

    else:
        return False


def delete_recursive(path):
    """
    Recursive delete files in dir
    :param path:absolute path to dir
    """
    _is_settings_created()
    path_to_file = os.path.abspath(path)

    if not _is_sys_dir(path_to_file):
        if os.path.isfile(path_to_file):
            os.remove(path_to_file)

        elif os.path.islink(path_to_file):
            os.unlink(path_to_file)

        else:
            for root, dirs, files in os.walk(path, topdown=False):
                for file_name in files:
                    os.remove(os.path.join(root, file_name))
                for dir_name in dirs:
                    os.rmdir(os.path.join(root, dir_name))
            os.rmdir(path_to_file)

    else:
        raise Exception("Permission denied")


def is_valid_trash(path_to_trash):
    """
    Check is trash congig exist
    :param path_to_trash: path to dir 
    :return: True if exist or False if not 
    """
    path_to_trash = os.path.abspath(path_to_trash)
    path_to_config = os.path.join(path_to_trash, ".config")
    return is_exist(path_to_config)


def create_config(path, content):
    """
    create or open and update settings in config file
    :param path: path to config
    :param content: info for config
    :return: True if config created, False if not
    """
    path_to_config = os.path.abspath(path)
    if not os.path.isfile(path_to_config) or os.path.getsize(path_to_config) == 0:
        with open(path, "w") as config:
            for key in content.iterkeys():
                setting = "{key}:{value}\n".format(key=key, value=content[key])
                config.write(setting)

        return True

    return False


def get_info_from_config(path):
    """
    Get settings from user config file
    :param path: path to user config file
    :return: settings from config file(dict)
    """
    _is_settings_created()
    config = {}
    path_to_config = os.path.abspath(path)

    with open(path_to_config) as config_file:
        content = config_file.readlines()
        for s in content:
            (key, value) = s.rstrip('\n').split(':')
            config[key] = value

        return config


def _load_info_json(path_to_trash):
    """
    Get trash's info from JSON file
    :return: info from JSON file
    """
    _is_settings_created()
    path_to_trash = os.path.abspath(path_to_trash)
    path_to_info = os.path.join(path_to_trash, ".info_json")

    if is_exist(path_to_info):
        trash_info = json.load(open(path_to_info, "r+"))

    else:
        with open(path_to_info, "w") as info_file:
            info_file.write("[]")
        trash_info = json.load(open(path_to_info), "r+")

    return trash_info


def _update_info_json(path_to_trash, path, uuid):
    """
    Update trash's file with info after some actions(delete or restore)
    :param path_to_trash: 
    :param path: path to file with will delete or restore
    :param uuid: uuid name of file with will delete or restore
    :return: None
    """
    path_to_file = os.path.abspath(path)
    path_to_trash = os.path.abspath(path_to_trash)
    info_json = _load_info_json(path_to_trash)
    new_file = {
        "UUID": uuid,
        "name": path_to_file.split('/')[-1],
        "file_path": path_to_file.rstrip(path_to_file.split('/')[-1]),
        "time": datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"),
    }
    info_json.append(new_file)
    _write_file_info_json(path_to_trash, info_json)


def _write_file_info_json(path_to_trash, info_js):
    """
    Change trash info
    :param path_to_trash: 
    :param info_js: JSON file
    :return:
    """
    _is_settings_created()
    trash_path = os.path.abspath(path_to_trash)
    json.dump(info_js, open(os.path.join(trash_path, ".info_json"), "w+"), indent=4)


def print_file(path):
    """
    Print file on the screen
    :param path: path to file
    """
    file_path = os.path.abspath(path)
    if os.path.isfile(file_path):
        with open(file_path, "r") as book:
            book = book.read()
        pydoc.pager(book)
