"""
Fibonachi Generator
"""


def fibinachi(numb):
    fib_num = 0
    prev = 0
    next = 1

    for val in range(0, numb):
        if val == 0:
           yield 1
        else:
            fib_num = prev + next
            prev, next = next, fib_num
            yield fib_num

if __name__ == "__main__":
    print "Input max fibonachi number"
    numb = int(raw_input())
    fib_gen = fibinachi(numb)
    print [val for val in fib_gen]
