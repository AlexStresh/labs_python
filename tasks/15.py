"""
Pattern Singleton
"""


class SingletonMeta(type):

    def __init__(cls, name, bases, dct):
        super(SingletonMeta, cls).__init__(name, bases, dct)
        cls.instance = None

    def __call__(self, *args, **kwargs):
        if self.instance is None:
            self.instance = super(SingletonMeta, self).__call__(*args, **kwargs)
        return self.instance


class TestClass(object):
    __metaclass__ = SingletonMeta

    def __init__(self, name):
        self.name = name

if __name__ == "__main__":
    a = TestClass('Stresh')
    print a.name
    b = TestClass('Saniok')
    print b.name
