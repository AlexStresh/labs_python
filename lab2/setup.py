#!/usr/bin/env python2.7

from setuptools import setup, find_packages
import os

setup(
    name="smrm",
    version="1.0",
    test_suite="tests",
    packages=find_packages(),
    include_packages_data=True,
    long_description="By using program smart rm you can delete file from your computer, like rm, but better",
    entry_points={
        "console_scripts":
            ["smrm = bin.smart_rm:main"]
    }
)