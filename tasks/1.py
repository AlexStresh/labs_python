"""
Task 1 - Text analise
"""

import os
import re


def medium_numb(file):
    all_word = 0
    string_numb = file.__len__()

    for string in file:
        dic = string.split(' ')
        all_word += dic.__len__()
    return int(all_word/string_numb)


def word_numb(file):
    word_dict = {}
    for string in file:
        string = [word for word in string.split(' ') if word != '']
        for word in string:
            word = word.strip().lower()
            if word in word_dict.keys():
                word_dict[word] = word_dict[word] + 1
            else:
                word_dict[word] = 1
    return word_dict


def start_analise(path_to_file):
    path = os.path.abspath(path_to_file)
    with open(path, 'r') as text:
        text_str = text.readlines()
    count = text_str.__len__()
    valid_str = []
    text_str = [re.sub("[^0-9A-Za-z .!?]", "", string).replace('\n', '') for string in text_str if string != '\n']
    text_str = [re.sub("[^0-9A-Za-z .]", ".", string) for string in text_str]

    while text_str:
        if not text_str:
            continue
        string = text_str.pop(0).strip()

        if string[-1] != '.':
            next_str = text_str.pop(0).strip()
            string = string + ' ' + next_str
            text_str.insert(0, string)
        else:
            valid_str.append(string)
    for parse_str in valid_str:
        [text_str.append(string) for string in parse_str.split('.') if string != '']
    text_str = [string.replace(".", "") for string in text_str]

    print medium_numb(text_str)
    print word_numb(text_str)


if __name__ == '__main__':
    print 'Input path to file'
    # path = raw_input()
    path = 'test_file'
    start_analise(path)
