"""
JSON converter
"""


def char_to_json(ch):
    string_literals = ["\\", "\"", "\'", "\b", "\f", "\n", "\r", "\t", "\v"]
    replace_literals = ["\\\\", "\\\"", "\'", "\\b", "\\f", "\\n", "\\r", "\\t", "\\v"]

    for elem in string_literals:
        if ch == elem:
            ch = replace_literals[string_literals.index(elem)]

    return ch


def to_json(obj, raise_unknown=False):
    ans = []

    if type(obj) == list or type(obj) == tuple:
        ans.append('[')
        for elem in obj:
            ans.append(to_json(elem))
            ans.append(', ')
        ans.append(']')

    elif type(obj) == int or type(obj) == long or type(obj) == bool or type(obj) == float:
        ans.append(str(obj))

    elif type(obj) == str:
        ans.append('\"')
        for ch in obj:
            ans.append(char_to_json(ch))
        ans.append('\"')

    elif type(obj) == dict:
        ans.append('{')
        for key, value in obj.items():
            if type(key) == int or type(key) == long or type(key) == bool or type(key) == float:
                ans.append('\"')
                ans.append(to_json(key))
                ans.append('\"')
            else:
                ans.append(to_json(key))
            ans.append(': ')
            ans.append(to_json(value))
            ans.append(',')

        ans.append('}')

    else:
        if raise_unknown:
            raise TypeError("{} can't convert to JSON format".format(str(obj)))

    return "".join(ans)

if __name__ == "__main__":
    test_1 = 12
    test_2 = 'Stresh'
    test_3 = [1, 2, 3, 'abc']
    test_4 = {'a': 1, 'c': 3}
    print to_json(test_1), to_json(test_2), to_json(test_3), to_json(test_4)
