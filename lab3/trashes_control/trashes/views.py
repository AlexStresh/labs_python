# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from multiprocessing import RLock

from django.contrib import auth
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import (
    render,
    get_object_or_404,
    render_to_response,
    redirect,
    get_list_or_404
)
from django.template.context_processors import csrf
from django.urls import reverse
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth import login, logout

from utils import (
    find_files_with_regex,
    match_file_type,
    get_filtered_files,
    get_dir_content,
    DEFAULT_PATH
)
from models import Trash, Tasks
from forms import AddForm, RestoreForm, MyRegistrationForm
from bin.trash import is_valid_trash


lock = RLock()


def auth_decorator(func):
    """
    Check if user login
    :param func: decorate function
    :return: True - if ligIn
            HttpResponseRedirect to login page if not
    """
    def wrapped(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(redirect_to='/login')
        else:
            return func(request, *args, **kwargs)
    return wrapped


def call_bad_request():
    """
    If page not found - call page 404
    :return: 
    """
    response = render_to_response('404.html')
    response.status_code = 404
    return response


def get_files(request, cur_dir):
    """
    Get dir content(files/dir) and pack it to JSON
    :param request: request from site page
    :param cur_dir: path to dir
    :return: Send JSON file
    """
    return HttpResponse(get_dir_content(cur_dir))


@auth_decorator
def delete(request, pk):
    """
    Delete dirs and files from system 
    :param request: request from site page
    :param pk: primary key - trash id
    :return: redirect to main page if request.POST
            render delete page if request.GET
    """
    files_to_delete = []
    trash_model = get_object_or_404(Trash, id=pk)
    files = get_filtered_files(DEFAULT_PATH)
    dir_context = match_file_type(files)

    if request.method == "POST":
        regex = request.POST['regex']
        try:
            dry_run = request.POST['dry-run']
        except:
            dry_run = False

        for key, value in request.POST.items():
            if key.startswith(DEFAULT_PATH):
                files_to_delete.append(value)

        if regex:
            files_to_delete += find_files_with_regex(regex, files_to_delete)

        lock.acquire()

        trash = trash_model.create_trashes()
        files_to_delete.sort(reverse=True)
        trash.dry_run = dry_run
        deleted_files = trash.move_to_trash_thread(files_to_delete)
        for file_name, commit in deleted_files.items():
            Tasks.objects.create_task(
                task=Tasks.TRASH_DELETE,
                trash=trash_model,
                files=file_name,
                status=Tasks.STATUS_COMPLETED,
                creator=request.user,
                commit=commit
            )

        lock.release()
        return HttpResponseRedirect(redirect_to='/')

    return render(request, 'delete.html', {
        "username": request.user.username,
        "title": "DELETE",
        "btn_name": "Delete",
        "dir_context": dir_context
    })


@auth_decorator
def add_delete_task(request, pk):
    """
    Add delete task to task manager
    :param request: request from site page
    :param pk: primary key - trash id
    :return: redirect to main page if request.POST
            render delete page if request.GET
    """
    files_to_delete = []
    files = get_filtered_files(DEFAULT_PATH)
    dir_context = match_file_type(files)

    if request.method == "POST":
        regex = request.POST['regex']
        try:
            dry_run = request.POST['dry-run']
        except:
            dry_run = False

        for key, value in request.POST.items():
            if key.startswith(DEFAULT_PATH):
                files_to_delete.append(value)

        if regex:
            files_to_delete += find_files_with_regex(regex, files_to_delete)

        for file_name in files_to_delete:
            Tasks.objects.create_task(
                task=Tasks.TRASH_DELETE,
                trash=get_object_or_404(Trash, id=pk),
                files=file_name,
                status=Tasks.STATUS_PAINTING,
                creator=request.user,
                commit='',
                dry_run=dry_run
            )

        return HttpResponseRedirect(redirect_to='/')

    return render(request, 'delete.html', {
        "username": auth.get_user(request).username,
        "btn_name": "Add Task",
        "title": 'Add Delete Task',
        "dir_context": dir_context
    })


@auth_decorator
def change_password(request):
    """
    Change user password from cabinet
    :param request: 
    :return: 
    """
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('/')
    else:
        form = PasswordChangeForm(request.user)

    return render(request, 'change_password.html', {
        'form': form,
        'username': auth.get_user(request).username
    })


@auth_decorator
def delete_all_task(request):
    """
    Get and delete from db all Tasks objects
    :param request: request from site page
    :return: if user not logIn - return render login page,
            else - render main page('/')
    """
    tasks = get_list_or_404(Tasks, creator=request.user)

    for task in tasks:
        task.delete()

    return HttpResponseRedirect(redirect_to='/')


@auth_decorator
def run_all_task(request):
    """
    Get all task with status 'Waiting' and run it
    :param request: request from site page
    :return: if user not logIn - return render login page,
            else - render main page ('/')
    """
    tasks_for_run = get_list_or_404(
        Tasks,
        status=Tasks.STATUS_PAINTING, creator=request.user
    )

    for task in tasks_for_run:
        task.run_task()

    return HttpResponseRedirect(redirect_to='/')


@auth_decorator
def run_task(request, pk):
    """
    Create trash and run action with this trash
    :param request: request from site page
    :param pk: primary key - object Task
    :return: if user not logIn - return render login page,
            else - render main page ('/')
    """
    task_model = get_object_or_404(
        Tasks,
        pk=pk,
        creator=request.user
    )
    task_model.run_task()

    return HttpResponseRedirect(redirect_to='/')


@auth_decorator
def delete_task(request, pk):
    """
    Delete Task object from db by primary key 
    :param request: request from site page
    :param pk: primary key - object Task
    :return: if user not logIn - return render login page,
             else - render main page ('/')
    """
    task_model = get_object_or_404(
        Tasks,
        pk=pk,
        creator=request.user
    )
    task_model.delete()

    return HttpResponseRedirect('/')


@auth_decorator
def open_about_page(request):
    """
    Open and render 'About' site page
    :param request: request from site page
    :return: if user not logIn - return render login page,
            else - render 'About' page
    """
    return render(request, 'about.html', {'username': request.user.username})


@auth_decorator
def render_help_page(request):
    """
    Open and render 'Help' site page
    :param request: request from site page
    :return: if user not logIn - return render login page,
            else - render 'Help' page
    """
    return render(request, 'help.html', {'username': request.user.username})


def register_user(request):
    """
    Create and save User object to db using MyRegistrationForm
    :param request: request from site page
    :return: if user not logIn - return render login page,
            else - render 'register' page
    """
    context = {}

    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(redirect_to='/')

    context.update(csrf(request))
    context['form'] = MyRegistrationForm(request.POST)

    return render(request, 'register.html', context)


@auth_decorator
def add_restore_task(request, pk):
    """
    Create and save Restore Task object to db
    :param request: request form site page
    :param pk: primary key - object Task
    :return: if user not logIn - return render login page,
             elif request.POST - return render main page,
             else return render 'restore' page
    """
    trash_model = get_object_or_404(Trash, pk=pk)
    form = RestoreForm(request.POST or None)
    trash = trash_model.create_trashes()

    if request.method == "POST" and form.is_valid():
        files_to_restore = [val for val in request.POST.getlist('file')]
        can_replace = form.cleaned_data["solve_problem"]

        try:
            dry_run = request.POST["dry-run"]
        except:
            dry_run = False

        trash.dry_run = dry_run

        if can_replace:
            task = Tasks.TRASH_RESTORE_WITH_REWRITE

        else:
            task = Tasks.TRASH_RESTORE_WITHOUT_REWRITE

        for file_name in files_to_restore:
            Tasks.objects.create_task(
                task=task,
                trash=trash_model,
                files=file_name,
                status=Tasks.STATUS_PAINTING,
                creator=request.user,
                commit="",
                dry_run=dry_run
            )

        return HttpResponseRedirect('/')

    return render(request, "restore.html", {
        'form': form,
        'trash': trash_model,
        'title': 'Add Tasks',
        'trash_content': trash.get_trash_content(),
        'status': 'Add Restore Task',
        'btn_name': 'Add Tasks'
    })


def login_user(request):
    """
    LogIn User by username and password 
    :param request: request from site page
    :return: if user already logIn - return render main site page,
             else - return render 'login' page
    """
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')

        else:
            log_error = 'There is no such user'
            return render(request, 'login.html', {'log_error': log_error})

    return render(request, 'login.html')


@auth_decorator
def logout_user(request):
    """
    LogOut user from current session
    :param request: request from site page
    :return: return render main page('/')
    """
    logout(request)
    return HttpResponseRedirect(redirect_to='/login')


@auth_decorator
def render_user_cabinet(request,):
    """
    Render user cabinet page
    :param request: request from site page
    :return: render user_cabinet.html
    """
    return render(request, 'user_cabinet.html', {
        'username': request.user.username,
        'user': request.user
    })


@auth_decorator
def open_trash_detail(request, pk):
    """
    Open and render 'trash' page with trash details
    :param request: request from site page
    :param pk: primary key - Trash object
    :return: if user not logIn - return render login page,
             else - return render 'trash' page
    """
    trash_model = get_object_or_404(Trash, pk=pk)

    if trash_model.date_politic:
        trash_model.clean_trash_by_date_policy(request.user)

    if request.method == 'POST':
        if not trash_model.date_politic:
            trash_model.date_politic = True

        else:
            trash_model.date_politic = False

        trash_model.save()

    return render(request, 'trash.html', {
        'trash': trash_model,
        'trash_content': trash_model.get_trash_content(),
        'username': request.user.username
    })


@auth_decorator
def clean_trash(request, pk):
    """
    Clean trash content - delete all files from trash
    :param request: request from site page
    :param pk: primary key - Trash object
    :return: if user not logIn - return render login page,
             else - return render 'trash' page
    """
    trash_model = get_object_or_404(Trash, pk=pk)
    trash_model.clean_trash()

    Tasks.objects.create_task(
        trash=trash_model,
        task=Tasks.TRASH_CLEAN,
        files='',
        status=Tasks.STATUS_COMPLETED,
        commit='Success',
        creator=request.user
    )

    return HttpResponseRedirect(reverse('trash_detail', args=[pk]))


@auth_decorator
def delete_trash(request, pk):
    """
    Get Trash object from request and delete from db
    :param request: request from site page
    :param pk: primary key - Trash object
    :return: if user not logIn - return render login page,
             else - return render main page ('/')
    """
    user = get_object_or_404(User, username=auth.get_user(request).username)
    trash_model = get_object_or_404(Trash, pk=pk)

    if trash_model.creator == user:
        trash_model.delete()

    return HttpResponseRedirect(redirect_to="/")


@auth_decorator
def edit_trash(request, pk):
    """ 
    Edit and rewrite trash settings 
    :param request: request from site page
    :param pk: primary key - Trash object
    :return: if user not logIn - return render login page,
             elif request.POST - return render main page('/'),
             else return render 'add_trash' page
    """
    trash_model = get_object_or_404(Trash, pk=pk)

    if request.method == "POST":
        form = AddForm(request.POST, instance=trash_model)

        if form.is_valid():
            trash_form = form.save(commit=False)
            trash_form.trash_name = trash_form.path_to_trash.split("/")[-1]
            trash_model.change_trash_settings()
            trash_form.save()

            return HttpResponseRedirect(redirect_to="/")

    else:
        trash_model = AddForm(instance=trash_model)

    return render(request, "add_trash.html", {
        'form': trash_model,
        'page_name': 'Edit Settings',
        'username': request.user.username
    })


@auth_decorator
def restore_files(request, pk):
    """
    Get trash object from request and restore files from this trash
    :param request: request from site page
    :param pk: primary key - Trash object
    :return: if user not logIn - return render login page,
             elif request.POST - return render main page('/'),
             else return render 'restore' page
    """
    trash_model = get_object_or_404(Trash, pk=pk)
    form = RestoreForm(request.POST or None)
    trash = trash_model.create_trashes()

    if request.method == "POST" and form.is_valid():
        files_to_restore = [val for val in request.POST.getlist('file')]
        can_replace = form.cleaned_data["solve_problem"]
        try:
            dry_run = request.POST['dry-run']
        except:
            dry_run = False

        trash.dry_run = dry_run
        restored_file = trash.restore_file_thread(files_to_restore, can_replace)

        if can_replace:
            task = Tasks.TRASH_RESTORE_WITH_REWRITE
        else:
            task = Tasks.TRASH_RESTORE_WITHOUT_REWRITE

        for file_name, commit in restored_file.items():
            Tasks.objects.create_task(
                task=task,
                trash=trash_model,
                files=file_name,
                status=Tasks.STATUS_COMPLETED,
                creator=request.user,
                commit=commit,
                dry_run=trash.dry_run
            )

    return render(request, "restore.html", {
        'form': form,
        'trash': trash_model,
        'btn_name': 'Restore',
        'status': 'Restore from: ' + trash_model.trash_name,
        'title': 'Restore Files',
        'trash_content': trash_model.get_trash_content(),
        'username': request.user.username
    })


@auth_decorator
def open_start_page(request):
    """
    Open and render main site page ('/') 
    :param request: request from site page
    :return: if user not logIn - return render login page,
             else return render 'trashes' page
    """
    trash_list = Trash.objects.all().filter(creator=request.user)
    task_list = Tasks.objects.all().filter(creator=request.user)

    for trash in trash_list:
        if is_valid_trash(trash.path_to_trash):
            trash.is_valid = True
        else:
            trash.is_valid = False

    return render(request, 'trashes.html', {
        'username': request.user.username,
        'trash_list': trash_list,
        'tasks_list': task_list
    })


@auth_decorator
def add_trash(request):
    """
    Add Trash - create trash model and save to db
    :param request: request from site page
    :return: if user not logIn - return render login page,
             elif request.POST - return render main page('/'),
             else return render 'add_trash' page
    """

    if request.method == "POST":
        form = AddForm(request.POST)

        if form.is_valid():
            trash_model = form.save(commit=False)
            trash_model.trash_name = trash_model.path_to_trash.split("/")[-1]
            trash_model.creator = request.user
            trash_model.save()
            trash_model.change_trash_settings()

            return HttpResponseRedirect(redirect_to="/")

    else:
        form = AddForm()

    return render(request, "add_trash.html", {
        "form": form,
        "username": request.user.username,
        "page_name": "Add Trash"
    })
